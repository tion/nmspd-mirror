//------------------------------------------------------------------
#include <QtCore/QPointer>
#include <QtCore/QSize>                // For glyph icons
#include <QPalette>                    // For glyph icons
#include <QPixmap>                     // For glyph icons
#include <QIcon>                       // For glyph icons
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QGroupBox>

#include "../include/PortalAddressPickerWindow.h"
#include "../include/CoordinateDataHelpers.h"
//------------------------------------------------------------------
const int TOTALGLYPHS = 16;
const int GLYPHCODE_LENGTH = 12;
const int TOTALGLYPHS_PER_ROW = 8;
const int SMALL_IMAGE_SIZE_IN_PX = 40;
const int MEDIUM_IMAGE_SIZE_IN_PX = 60;
//------------------------------------------------------------------
namespace NMS {
//------------------------------------------------------------------
PortalDialerWidget::PortalDialerWidget (const QString &_code,
                                        QWidget *parent)
   : QWidget (parent)
{
   glyph_select.reserve (TOTALGLYPHS);

   glyphcode_line = new QLineEdit;
   glyphcode_line -> setMaxLength (GLYPHCODE_LENGTH);
   glyphcode_line -> setReadOnly (true);
   glyphcode_line -> setToolTip (QStringLiteral("Glyph Code"));

   for (int i = 0; i < TOTALGLYPHS; i++)
      glyph_select.append (new QToolButton);

   for (int i = 0; i < TOTALGLYPHS; i++)
   {
      glyph_select[i] -> setDisabled (false);
      glyph_select[i] -> setBaseSize (QSize (SMALL_IMAGE_SIZE_IN_PX, SMALL_IMAGE_SIZE_IN_PX));
      glyph_select[i] -> setMaximumSize (QSize (MEDIUM_IMAGE_SIZE_IN_PX, MEDIUM_IMAGE_SIZE_IN_PX));
      glyph_select[i] -> setBackgroundRole (QPalette::Mid);
      glyph_select[i] -> setCheckable (false);
      glyph_select[i] -> setAutoRaise (true);
      glyph_select[i] -> setIcon (QIcon (QPixmap (QString (QStringLiteral(":") + 
                                             convert_integer_to_hex (i) + 
                                             QStringLiteral(".jpg")))));
      glyph_select[i] -> setIconSize (glyph_select[i] -> size());
      glyph_select[i] -> setToolTip (QString (QStringLiteral("Glyph ") + 
                                             QString::number (i + 1)));
   }

   // Import glyph code
   if (!_code.isEmpty())
      setGlyphCode (_code);

   QPointer<QPushButton> delete_button = new QPushButton;
   delete_button -> setIcon (QIcon::fromTheme (QStringLiteral("edit-undo")));
   delete_button -> setToolTip (QStringLiteral("Delete one digit to the left"));
   delete_button -> setCheckable (false);

   if (delete_button -> icon().isNull())
      delete_button -> setText (QStringLiteral ("-"));

   QPointer<QPushButton> clear_button = new QPushButton;
   clear_button -> setIcon (QIcon::fromTheme (QStringLiteral("edit-clear")));
   clear_button -> setToolTip (QStringLiteral("Reset glyph code entered"));
   clear_button -> setCheckable (false);

   if (clear_button -> icon().isNull())
      clear_button -> setText (QStringLiteral ("--"));

   QPointer<QGroupBox> buttonGroup = new QGroupBox;
   QPointer<QHBoxLayout> buttons = new QHBoxLayout;
   buttons -> addWidget (delete_button);
   buttons -> addWidget (clear_button);
   QPointer<QVBoxLayout> buttonLayout = new QVBoxLayout;
   buttonLayout -> addLayout (buttons);
   buttonLayout -> addWidget (glyphcode_line);
   buttonGroup -> setLayout (buttonLayout);

   QPointer<QHBoxLayout> box_row1 = new QHBoxLayout;

   for (int i = 0; i < TOTALGLYPHS_PER_ROW; i++)
   {
      box_row1 -> addWidget (glyph_select[i]);
   }

   QPointer<QHBoxLayout> box_row2 = new QHBoxLayout;

   for (int i = TOTALGLYPHS_PER_ROW; i < TOTALGLYPHS; i++)
   {
      box_row2 -> addWidget (glyph_select[i]);
   }

   QPointer<QGroupBox> boxGroup = new QGroupBox;
   QPointer<QVBoxLayout> box = new QVBoxLayout;
   box -> addLayout (box_row1);
   box -> addLayout (box_row2);
   boxGroup -> setLayout (box);

   QPointer<QHBoxLayout> mainLayout = new QHBoxLayout;
   mainLayout -> addWidget (buttonGroup);
   mainLayout -> addWidget (boxGroup);

   setLayout (mainLayout);

   /*
    * Handle pushing buttons
    */
   connect (delete_button, &QPushButton::clicked, this, &PortalDialerWidget::removeLast);
   connect (clear_button, &QPushButton::clicked, this, &PortalDialerWidget::clear);

   /*
    * Selecting glyph images
    */
   for (int i = 0; i < TOTALGLYPHS; i++)
   {
      connect (glyph_select[i], &QPushButton::clicked, this, [=]() { addGlyphAtIndex (i); });
   }

   connect (this, &PortalDialerWidget::codeStatus, this, [=](bool status) { toggleGlyphInput (status); });
   connect (glyphcode_line, &QLineEdit::textChanged, this, [=](const QString &newText)
   {
      if (newText.isEmpty())
         emit codeStatus (false);
   });
}
//------------------------------------------------------------------
PortalDialerWidget::~PortalDialerWidget()
{
   // Pointer clean-up
   while (glyph_select.count() > 0)
   {
      delete glyph_select[0];
      glyph_select.remove (0);
   }

   glyphcode_line -> setReadOnly (false);
   delete glyphcode_line;
}
//------------------------------------------------------------------
//////////////////////
// PUBLIC FUNCTIONS //
//////////////////////
//------------------------------------------------------------------
QString PortalDialerWidget::getGlyphCode() const
{
   return (glyphcode_line -> text());
}
//------------------------------------------------------------------
//////////////////
// PUBLIC SLOTS //
//////////////////
//------------------------------------------------------------------
void PortalDialerWidget::removeLast()
{
   if (glyphcode_line -> text().isEmpty())
      return;

   QString newCode (glyphcode_line -> text().left (glyphcode_line -> text().size() - 1));
   glyphcode_line -> setText (newCode);

   emit codeChanged();
   emit codeStatus (false);
}
//------------------------------------------------------------------
void PortalDialerWidget::clear()
{
   if (glyphcode_line -> text().isEmpty())
      return;

   glyphcode_line -> clear();
   emit codeChanged();
   emit codeStatus (false);
}
//------------------------------------------------------------------
void PortalDialerWidget::addGlyphAtIndex (const int index)
{
   if (glyphcode_line -> text().size() >= 0
      && glyphcode_line -> text().size() < GLYPHCODE_LENGTH)
   {
      glyphcode_line -> insert (QString (convert_integer_to_hex (index).at (0).toUpper()));
      emit codeChanged();
   }

   // Update status for any change
   if (glyphcode_line -> text().size() == GLYPHCODE_LENGTH)
      emit codeStatus (true);
   else
   {
      emit codeStatus (false);
      emit codeChanged();
   }
}
//------------------------------------------------------------------
void PortalDialerWidget::setGlyphCode (const QString &_code)
{
   if (_code.isEmpty() || _code.size() > GLYPHCODE_LENGTH)
      return;

   QString fileName = QString();

   if (is_valid_glyph_code (_code))
   {
      clear();
      glyphcode_line -> insert (_code);
      emit codeChanged();
      emit codeStatus (true);
   }
   else if (!glyphcode_line -> text().isEmpty()
            && _code.toLower() != glyphcode_line -> text().toLower())
   {
      clear();
      QString temp = QString();
      const QRegularExpression validHex (QStringLiteral("[0-9A-F]"));
      QRegularExpressionMatch isValidHex;

      for (int i = 0; i < GLYPHCODE_LENGTH; i++)
      {
         if (_code.isEmpty())
            fileName = QStringLiteral(":") + QString (_code.at (i)) + QStringLiteral(".jpg");
         else
         {
            isValidHex = validHex.match (_code.at (i), Qt::CaseInsensitive);

            if (isValidHex.hasMatch())
            {
               temp += _code.at (i).toUpper();
               fileName = QStringLiteral(":") + QString (_code.at (i)) + QStringLiteral(".jpg");
            }
         }
      }

      glyphcode_line -> insert (temp);
      emit codeChanged();

      if (glyphcode_line -> text().size() == GLYPHCODE_LENGTH)
         emit codeStatus (true);
   }
   else
      emit codeStatus (false);
}
//------------------------------------------------------------------
///////////////////
// PRIVATE SLOTS //
///////////////////
//------------------------------------------------------------------
void PortalDialerWidget::toggleGlyphInput (bool status)
{
   for (int i = 0; i < TOTALGLYPHS; i++)
      glyph_select[i] -> setDisabled (status);
}
//------------------------------------------------------------------
}  // end of namespace: NMS
