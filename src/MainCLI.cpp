#include <QCoreApplication>
#include <QCommandLineParser>
#include <QTextStream>

#include "config.h"
#include "../include/CoordinateData.h"

inline void setSettings()
{
   QCoreApplication::setOrganizationName (QString());
   QCoreApplication::setApplicationName (QString (APPLICATION_NAME));
   QCoreApplication::setApplicationVersion (QStringLiteral(PROGRAM_VERSION));
   QLocale::setDefault(QLocale::c());
}

int main (int argc, char *argv[])
{
   if (argc <= 0)
      return (EXIT_FAILURE);

   QCoreApplication app (argc, argv);
   setSettings();

   QCommandLineParser parser;
   parser.setApplicationDescription ("Converts coordinates to a portal glyph address, and vice versa.");
   parser.addHelpOption();
   parser.addVersionOption();
   parser.addPositionalArgument ("code", "Coordinate or glyph code sequence to convert. Multiples must be space-separated");

   QCommandLineOption verboseOption (QStringList() << "V" << "verbose", "Give more information about errors.");
   parser.addOption (verboseOption);

   parser.process (app);
   const QStringList args = parser.positionalArguments();
   bool verbosity = parser.isSet (verboseOption);
   QTextStream output (stdout);
   QTextStream input;
   QTextStream error (stderr);
   CoordinateData readThis = CoordinateData();
   QString parseThis = QString();

   for (int i = 0; i < args.size(); i++)
   {
      parseThis = args.at (i);

      if (parseThis.size() >= 1)
      {
         readThis.reset();
         input.setString (&parseThis);
         input >> readThis;

         if (readThis.isEmpty() == false && readThis.getCoordinate().isEmpty() == false)
         {
            readThis.convertToGlyph();

            if (readThis.getGlyphCode().isEmpty() == false)
               output << readThis.getGlyphCode() << "\n";
            else
            {
               if (verbosity == true)
               {
                  if (readThis.getErrorInfo().isEmpty() == false)
                     error << readThis.getCoordinate() << " invalid: " << readThis.showErrorInfo();
               }
            }
         }
         else
         {
            readThis.convertToCoordinate();

            if (readThis.getCoordinate().isEmpty() == false)
               output << readThis.getCoordinate() << ", portal #" << readThis.getPortal() << "\n";
            else
            {
               if (verbosity == true)
               {
                  if (readThis.getErrorInfo().isEmpty() == false)
                     error << readThis.getGlyphCode() << " invalid: " << readThis.showErrorInfo();
               }
            }
         }
      }
   }

   return (EXIT_SUCCESS);
}
