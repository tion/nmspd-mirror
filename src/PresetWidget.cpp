//------------------------------------------------------------------
#include <QSettings>
#include <QtCore/QStandardPaths>
#include <QtCore/QPointer>
#include <QSqlField>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QSqlQueryModel>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QInputDialog>
#include <QtWidgets/QFileDialog>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include "config.h"
#include "../include/PresetWidget.h"
#include "../include/CoordinateDataHelpers.h"
//------------------------------------------------------------------
const int GLYPHCODE_LENGTH = 12;
const int COORDINATE_LENGTH = 19;
const int PORTAL_MAX = 16;
//------------------------------------------------------------------
namespace NMS {
//------------------------------------------------------------------
PresetWidget::PresetWidget (QWidget *parent)
   : QWidget (parent)
{
   /*
    * Set up database
    */
   presetModel = nullptr;
   initDB();
   presetModel -> setTable (QStringLiteral("preset"));
   presetModel -> setEditStrategy (QSqlTableModel::OnFieldChange);
   presetModel -> select();
   presetModel -> setHeaderData (0, Qt::Horizontal, QStringLiteral("ID"));
   presetModel -> setHeaderData (1, Qt::Horizontal, QStringLiteral("Abbreviation"));
   presetModel -> setHeaderData (2, Qt::Horizontal, QStringLiteral("Coordinate"));
   presetModel -> setHeaderData (3, Qt::Horizontal, QStringLiteral("Description"));

   /*
    * Setup window layout
    */
   QPointer<QPushButton> deleteButton = new QPushButton;
   deleteButton -> setCheckable (false);
   deleteButton -> setToolTip (QStringLiteral("Remove selected preset"));
   deleteButton -> setStatusTip (QStringLiteral("Remove preset"));
   deleteButton -> setIcon (QIcon::fromTheme (QStringLiteral("edit-delete")));

   if (deleteButton -> icon().isNull())
      deleteButton -> setText (QStringLiteral("-"));

   QPointer<QPushButton> editButton = new QPushButton;
   editButton -> setIcon (QIcon::fromTheme (QStringLiteral("accessories-text-editor")));
   editButton -> setCheckable (false);
   editButton -> setToolTip (QStringLiteral("Edit selected preset"));
   editButton -> setStatusTip (QStringLiteral("Edit preset"));

   if (editButton -> icon().isNull())
      editButton -> setText (QStringLiteral("..."));

   QPointer<QPushButton> clearButton = new QPushButton;
   clearButton -> setIcon (QIcon::fromTheme (QStringLiteral("edit-clear")));
   clearButton -> setCheckable (false);
   clearButton -> setToolTip (QStringLiteral("Remove all presets"));
   clearButton -> setStatusTip (QStringLiteral("Clear all presets"));

   if (clearButton -> icon().isNull())
      clearButton -> setText (QStringLiteral("--"));

   QPointer<QComboBox> preset_selector = new QComboBox;
   preset_selector -> setToolTip (QStringLiteral("Select a preset"));

   QPointer<QLabel> descriptionLabel = new QLabel;
   descriptionLabel -> setTextFormat(Qt::AutoText);
   descriptionLabel -> setWordWrap (true);
   descriptionLabel -> setAlignment (Qt::AlignCenter);

   QPointer<QHBoxLayout> listLayout = new QHBoxLayout;
   listLayout -> addWidget (preset_selector);
   listLayout -> addWidget (editButton);
   listLayout -> addWidget (deleteButton);
   listLayout -> addWidget (clearButton);
   listLayout -> setSpacing (1);

   // Stretch preset_selector to match(0)/exceed(1) length of text
   if (! listLayout -> setStretchFactor (preset_selector, 1))       // Qt 4.5
      qDebug() << "[PresetWidget] Unable to adjust QComboBox stretch factor";

   QPointer<QVBoxLayout> mainLayout = new QVBoxLayout;
   mainLayout -> addLayout (listLayout);
   mainLayout -> addWidget (descriptionLabel);

   setLayout (mainLayout);

   /*
    * Connections/Event handling
    */
   // Apply/update changes to database
   connect (this, &PresetWidget::presetChanged, this, &PresetWidget::updatePresets);

   // Handle button press: remove presets
   connect (deleteButton, &QPushButton::clicked, this, [=]()
   {
      const int index = preset_selector -> currentIndex();

      if (count() > 0 && index >= 0)
      {
         const QString deleteThisAbbr = preset_selector -> itemText(index);
         setStatusTip (QStringLiteral ("Removing: ") + deleteThisAbbr);
         presetModel -> removeRows (index, 1);
         emit presetChanged();
         emit errorMessage (QString(QStringLiteral("Delete on index: ") + QString::number(index) +
                           QStringLiteral("of preset") + deleteThisAbbr));
         emit selectionChange(QString(), QStringLiteral("delete"), index);
      }
   });

   // Handle button press: reset/clear all values
   connect (this, &NMS::PresetWidget::resetSignal, this, [=]()
   {
      preset_selector -> clear();
   });
   connect (clearButton, &QPushButton::clicked, this, &NMS::PresetWidget::clear);

   // Add connections for each preset selection
   connect (preset_selector, QOverload<int>::of(&QComboBox::activated), this, &PresetWidget::selectPreset);

   // Offer some feedback for preset selection
   connect (preset_selector, QOverload<int>::of(&QComboBox::highlighted), this, [=](int index)
   {
      QSqlRecord highlightedRecord(recordAt (index));

      if (highlightedRecord.isEmpty())
         return;

      emit descriptionChange(highlightedRecord.value("description").toString());
      setStatusTip (highlightedRecord.value("coordinate").toString());
   });

   connect (editButton, &QPushButton::clicked, this, [=]()
   {
      editAt (preset_selector -> currentIndex());
   });

   // Update changes to QLabel
   connect (this, &PresetWidget::descriptionChange, descriptionLabel, &QLabel::setText);

   // Update changes to QComboBox
   connect (this, &PresetWidget::selectionChange, this, [=](const QString &presetAbbr, const QString &performAction, const int index)
   {
      if (performAction == QStringLiteral ("add"))
         preset_selector -> addItem(presetAbbr);
      else if (performAction == QStringLiteral ("delete"))
      {
         if (index < 0 || index >= preset_selector -> count())
            return;

         // Remove whatever item/preset abbreviation is at index
         preset_selector -> removeItem(index);
      }
      else if (performAction == QStringLiteral ("change"))
      {
         if (isValidIndex(index))
            preset_selector -> setItemText(index, presetAbbr);
      }
      else { }
   });

   buildPresetList();
}
//------------------------------------------------------------------
PresetWidget::~PresetWidget()
{
   delete presetModel;
}
//------------------------------------------------------------------
//////////////////////
// PUBLIC FUNCTIONS //
//////////////////////
//------------------------------------------------------------------
QSqlRecord PresetWidget::recordAt (const int index) const
{
   if (!isValidIndex (index))
   {
      qDebug() << "[PresetWidget][recordAt] Invalid index:" << index;
      return (QSqlRecord());
   }

   return (presetModel -> record (index));
}
//------------------------------------------------------------------
int PresetWidget::count() const
{
   int totalRows = 0;
   QSqlQuery query (QStringLiteral("SELECT COUNT(*) FROM preset"));
   query.first();
   totalRows = query.value(0).toInt();

   return (totalRows);
}
//------------------------------------------------------------------
//////////////////
// PUBLIC SLOTS //
//////////////////
//------------------------------------------------------------------
void PresetWidget::addPreset (const QString &coords, const QString &abbr, const QString &description, const bool skipEdit)
{
   if (coords.isEmpty() || abbr.isEmpty() || description.isEmpty())
   {
      emit errorMessage (QStringLiteral("[PresetWidget][addPreset] Missing key data fields. Please check that inputs are not empty"));
      return;
   }

   // Assumes valid coordinate and/or glyph code.
   const int dbSize = this -> count();
   QSqlRecord recordToAdd = QSqlRecord();
   recordToAdd.append (QSqlField ("id", QVariant::Int));
   recordToAdd.append (QSqlField ("coordinate", QVariant::String));
   recordToAdd.append (QSqlField ("abbreviation", QVariant::String));
   recordToAdd.append (QSqlField ("description", QVariant::String));
   recordToAdd.setValue ("coordinate", coords);
   recordToAdd.setValue ("abbreviation", abbr);
   recordToAdd.setValue ("description", description);

   if (recordToAdd.isEmpty())
   {
      emit errorMessage (QStringLiteral("[PresetWidget][addPreset] Something is VERY wrong. Attempting to add empty data..."));
      return;
   }

   if (presetModel -> insertRecord (dbSize, recordToAdd))
   {
      emit presetChanged();
      emit selectionChange(abbr, QStringLiteral("add"), -1);
   }
   else
   {
      emit errorMessage (QString(QStringLiteral("[PresetWidget][addPreset] Something happened while adding:\n") +
                                          coords + QStringLiteral("\n") + abbr + QStringLiteral("\n") + description));
   }

   if (!skipEdit)
      editAt(dbSize);
}
//------------------------------------------------------------------
void PresetWidget::selectPreset (const int index)
{
   QSqlRecord activatedRecord(recordAt (index));

   if (activatedRecord.isEmpty())
      return;

   CoordinateData temp = CoordinateData();
   QString coords = activatedRecord.value("coordinate").toString();

   if (coords.size() >= COORDINATE_LENGTH)
   {
      temp.setCoordinate (coords.section (',', 0, 0));

      if (!coords.section (',', 1, 1).isEmpty())
         temp.setPortal (coords.section('#', 1, 1).toInt());
      else
         temp.setPortal (1);

      temp.convertToGlyph();
      emit preset (temp);
   }
   else if (coords.size() == GLYPHCODE_LENGTH)
   {
      temp.setGlyphCode (coords);
      temp.convertToCoordinate();
      emit preset (temp);
   }
   else
   {
      emit errorMessage (QStringLiteral("[PresetWidget][selectPreset] Preset data is invalid! Nothing to show"));
   }
}
//------------------------------------------------------------------
void PresetWidget::clear()
{
   QSqlQuery query (QStringLiteral("DELETE FROM preset WHERE id > -1"));
   setStatusTip (QString());
   emit descriptionChange(QString());
   emit presetChanged();
   emit resetSignal();
}
//------------------------------------------------------------------
void PresetWidget::exportAt (const int index)
{
   if (!isValidIndex (index))
   {
      emit errorMessage (QString(QStringLiteral("[PresetWidget][exportAt] Invalid index: ") + QString::number(index)));
      return;
   }

   QSqlRecord result(recordAt (index));

   emit exportPreset (result.value("abbreviation").toString(), 
      result.value("coordinate").toString(), 
      result.value("description").toString());
}
//------------------------------------------------------------------
void PresetWidget::edit (const QString &editThisAbbr)
{
   editAt (indexOf (editThisAbbr));
}
//------------------------------------------------------------------
void PresetWidget::toFile (const QString &file)
{
   qDebug() << "[PresetWidget][toFile]";

   if (count() <= 0)
   {
      qInfo() << "No presets to export";
      return;
   }

   QString _file = file;

   if (_file.isEmpty())
   {
      qInfo() << "No initial file provided. Opening QFileDialog";

      QStringList fileNames;
      const QString path = QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);

      QFileDialog file_dialog (this, QStringLiteral("Save XML File"), path, QStringLiteral("XML files (*.xml);;All files (*)"));
      file_dialog.setAcceptMode (QFileDialog::AcceptSave);
      file_dialog.setViewMode (QFileDialog::List);
      int result = file_dialog.exec();

      if (result)
      {
         fileNames = file_dialog.selectedFiles();

         if (!fileNames.isEmpty())
            _file = fileNames.at(0);
         else
            _file = QString();
      }

      if (_file.isEmpty())
         return;
   }

   QFile fileData (_file);

   if (fileData.open(QFile::WriteOnly))
   {
      QXmlStreamWriter writeToXML(&fileData);
      writeToXML.setAutoFormatting(true);
      writeToXML.writeStartDocument();
      writeToXML.writeDTD(QStringLiteral("<!DOCTYPE presetdb>"));
      writeToXML.writeStartElement(QStringLiteral("PRESETTOPLEVELBLOCK"));
      writeToXML.writeComment(QStringLiteral(" Added: ") + QDateTime::currentDateTime().toString() + QStringLiteral(" "));

      int presetsExported = 0;
      const int totalRecords = this -> count();
      QString tempCode = QString();
      QString tempAbbr = QString();
      QString tempDescr = QString();
      QSqlRecord recordToSave = QSqlRecord();

      for (int i = 0; i < totalRecords; i++)
      {
         recordToSave = recordAt (i);
         tempCode = recordToSave.value("coordinate").toString();
         tempAbbr = recordToSave.value("abbreviation").toString();
         tempDescr = recordToSave.value("description").toString();

         writeToXML.writeStartElement("PRESET");   // start preset block
         writeToXML.writeTextElement("COORDINATE", tempCode);
         writeToXML.writeTextElement("ABBREVIATION", tempAbbr);
         writeToXML.writeTextElement("DESCRIPTION", tempDescr);
         writeToXML.writeEndElement(); // end preset block
         presetsExported++;
      }

      if (presetsExported == totalRecords)
         qInfo() << "Saved all" << presetsExported << "presets to:" << QDir::toNativeSeparators(_file);
      else
         emit errorMessage (QString(QStringLiteral("Something happened exporting presets to file. ") + QString::number(presetsExported) + QStringLiteral("were saved")));

      writeToXML.writeEndDocument();
   }
   else
      emit errorMessage (QString(QStringLiteral("Error writing to the file: ") + file + QStringLiteral("\nDo you have correct permissions?")));

   fileData.close();
}
//------------------------------------------------------------------
void PresetWidget::fromFile (const QString &file)
{
   qDebug() << "[PresetWidget][fromFile]";
   QString _file = file;

   if (_file.isEmpty())
   {
      qInfo() << "No initial file provided. Opening QFileDialog";

      QStringList fileNames;
      const QString path = QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);

      QFileDialog file_dialog (this, QStringLiteral("Open XML File"), path, QStringLiteral("XML files (*.xml);;All files (*)"));
      file_dialog.setFileMode(QFileDialog::ExistingFile);
      file_dialog.setViewMode (QFileDialog::List);
      int result = file_dialog.exec();

      if (result)
      {
         fileNames = file_dialog.selectedFiles();

         if (!fileNames.isEmpty())
            _file = fileNames.at(0);
         else
            _file = QString();
      }

      if (_file.isEmpty())
         return;
   }

   QFile fileData (_file);

   if (fileData.open (QFile::ReadOnly))
   {
      QXmlStreamReader readFromXML(&fileData);

      int presetsAdded = int(0);
      int _portal = int(1);
      QString presetCode = QString();

      // Parse the file
      while (!readFromXML.atEnd())
      {
         readFromXML.readNextStartElement();    // Fetch new start element(tag)

         /*
          * PRESET block requires the form:
          *
          *  <PRESET>
          *     <COORDINATE>YourCoordinateValueHere</COORDINATE>
          *     <ABBREVIATION>Incredibly, Succinctly, Shortname</ABBREVIATION>
          *     <DESCRIPTION>A cool, nay, AMAZING description of epic proportions here</DESCRIPTION>
          *  </PRESET>
          */
         if ((readFromXML.name() == QStringLiteral("PRESET") 
           || readFromXML.name() == QStringLiteral("preset")) 
           && (!readFromXML.isEndElement()))
         {
            QString tempCoords = QString();
            QString tempAbbr = QString();
            QString tempDesc = QString();
            readFromXML.readNextStartElement();    // Go to next element(attribute)

            if (readFromXML.name() == QStringLiteral("COORDINATE") 
              || readFromXML.name() == QStringLiteral("coordinate"))
            {
               tempCoords = readFromXML.readElementText();
            }

            readFromXML.readNextStartElement();

            if (readFromXML.name() == QStringLiteral("ABBREVIATION") 
              || readFromXML.name() == QStringLiteral("abbreviation"))
            {
               tempAbbr = readFromXML.readElementText();
            }

            readFromXML.readNextStartElement();

            if (readFromXML.name() == QStringLiteral("DESCRIPTION") 
              || readFromXML.name() == QStringLiteral("description"))
            {
               tempDesc = readFromXML.readElementText();
            }

            // Verify the data obtained is acceptable
            QString checkCoords = tempCoords.section(',', 0, 0);

            if (checkCoords.size() == COORDINATE_LENGTH || checkCoords.size() == GLYPHCODE_LENGTH)
            {
               if (is_valid_coordinate(checkCoords)
                 || is_acceptable_glyph_code(checkCoords))
               {
                  // Ensure coordinate field has proper format
                  if (tempCoords.contains("portal"))
                  {
                     presetCode = checkCoords.toUpper() + QStringLiteral(", portal #");

                     if (is_valid_portal(tempCoords.section('#', 1, 1).toInt()))
                        presetCode += tempCoords.section('#', 1, 1);
                     else
                        presetCode += QString::number(int(1));
                  }
                  else
                  {
                     presetCode = tempCoords.toUpper();

                     if (tempCoords.size() == COORDINATE_LENGTH)
                        presetCode += QStringLiteral(", portal #1");
                  }

                  // Add non-empty text for abbreviation
                  if (tempAbbr.isEmpty())
                     tempAbbr = QStringLiteral("New Preset for ") + presetCode;

                  // Add non-empty text for description
                  if (tempDesc.isEmpty())
                     tempDesc = QStringLiteral("No description.");

                  // Add to current database
                  presetsAdded++;
                  addPreset(presetCode, tempAbbr, tempDesc);
               }
            }
         }  // end of PRESET block
      }

      qInfo() << "Found" << presetsAdded << "presets in file:" << _file;

      if (presetsAdded > 0)
         emit presetChanged();
   }

   fileData.close();
}
//------------------------------------------------------------------
///////////////////////
// PRIVATE FUNCTIONS //
///////////////////////
//------------------------------------------------------------------
void PresetWidget::initDB()
{
   QSettings settings(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + QDir::separator() + QStringLiteral("settings.conf"), QSettings::IniFormat);
   qDebug() << "[PresetWidget][initDB]";

   if (!settings.contains ("dbpath")
      || settings.value("dbpath").toString().isEmpty())
   {
      // Set a default db path: first run or otherwise empty current path in config file)
      const QString db_path = QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);

      // Create directory structure for db_path if it does not exist
      QDir createNewPath(db_path);

      if (!createNewPath.exists())
      {
         createNewPath.mkpath(db_path);

         if (createNewPath.exists())
            qDebug() << "Created path to:" << db_path;
         else
            qCritical() << "Failed creating path:" << db_path << "\nMissing permissions?";
      }

      QString fullpath = db_path + QDir::separator() + QStringLiteral(APPLICATION_NAME) + QStringLiteral(".presets");
      qInfo() << "Setting default db_path:" << QDir::toNativeSeparators(fullpath);
      settings.setValue("dbpath", fullpath);
   }

   QString path = settings.value("dbpath").toString();
   QFile newDB (path);

   if (!newDB.exists())
   {
      if (newDB.open(QIODevice::NewOnly))
         qDebug() << "Success opening new file:" << QDir::toNativeSeparators(path);
      else
      {
         qCritical() << "Unable to open new file:" << QDir::toNativeSeparators(path) << "\nMissing permissions?";
      }

      newDB.close();
   }
   else
   {
      // File already exists: do nothing
   }

   // Create Sqlite3 database connection
   QSqlDatabase db = QSqlDatabase::addDatabase (QStringLiteral("QSQLITE"));
   db.setDatabaseName (path);

   if (!db.open())
   {
      qDebug() << "An error occurred attempting to open:" << QDir::toNativeSeparators(path);
      qDebug() << db.lastError();
   }
   else
   {
      db = QSqlDatabase::database();
      qDebug() << "Database connection established:" << db.databaseName();

      // Create proper table "preset" if it does not exist
      QSqlQuery query;
      bool ok = query.exec(QStringLiteral("create table preset "
                "(id integer primary key, "
                "abbreviation text, "
                "coordinate text, "
                "description text)"));

      if (ok)
      {
         qInfo() << "Table initialized. Database ready:" << db.databaseName();
      }
      else
      {
         if (!query.lastError().databaseText().contains ("exists"))
            qCritical() << query.lastError();
         else
            qInfo() << "Database ready:" << db.databaseName();
      }
   }

   presetModel = new QSqlTableModel (this, db);
}
//------------------------------------------------------------------
///////////////////
// PRIVATE SLOTS //
///////////////////
//------------------------------------------------------------------
void PresetWidget::buildPresetList()
{
   QSqlQueryModel model;
   model.setQuery (QStringLiteral("SELECT * FROM preset"));
   QString tempCode = QString();

   for (int i = 0; i < model.rowCount(); ++i)
   {
      tempCode = model.record(i).value("coordinate").toString();

      if (!tempCode.isEmpty())
         emit selectionChange(model.record(i).value("abbreviation").toString(), QStringLiteral("add"), i);
   }
}
//------------------------------------------------------------------
int PresetWidget::indexOf (const QString &findThisPreset)
{
   int index = -1;
   QSqlQueryModel model;
   model.setQuery (QString (QStringLiteral("SELECT abbreviation FROM preset WHERE abbreviation=") + findThisPreset));

   for (int i = 0; i < model.rowCount(); i++)
   {
      if (model.record(i).value("coordinate").toString() == findThisPreset)
      {
         index = model.record(i).value("id").toInt();
         break;
      }
   }

   qDebug() << "[PresetWidget] IndexOf:" << index << "--" << findThisPreset;
   return (index);
}
//------------------------------------------------------------------
void PresetWidget::editAt (const int index)
{
   QSqlRecord recordToChange(recordAt (index));

   if (recordToChange.isEmpty())
   {
      emit errorMessage (QString(QStringLiteral("[PresetWidget][editAt] No record found at index: ") + QString::number(index)));
      return;
   }

   QString old_coords = recordToChange.value("coordinate").toString();
   QString old_abbr = recordToChange.value("abbreviation").toString();
   QString old_description = recordToChange.value("description").toString();
   QString new_coords = QString();
   QString new_code = QString();
   QString new_abbr = QString();
   QString new_description = QString();

   /*
    * Draw dialog box prompting to modify:
    *  - Coordinate data
    *  - Abbreviation
    *  - Description
    */
   QPointer<QDialog> editPresetDialog = new QDialog(this);
   editPresetDialog -> setWhatsThis(QStringLiteral("Modify the current preset values: coordinate, abbreviation, and description"));
   QPointer<QLineEdit> coordLine = new QLineEdit(old_coords);
   coordLine -> setWhatsThis(QStringLiteral("Modify the coordinate data of this preset. Accepts coordinate+portal OR glyph code"));
   coordLine -> setToolTip(coordLine -> whatsThis());
   QPointer<QLineEdit> abbrLine = new QLineEdit(old_abbr);
   abbrLine -> setWhatsThis(QStringLiteral("Modify the abbreviation of this preset"));
   abbrLine -> setToolTip(abbrLine -> whatsThis());
   QPointer<QTextEdit> descrBox = new QTextEdit(old_description.toHtmlEscaped());
   descrBox -> setWhatsThis(QStringLiteral("Modify the description of this preset. Some HTML tags are acceptable"));
   descrBox -> setToolTip(descrBox -> whatsThis());
   descrBox -> setWordWrapMode (QTextOption::WordWrap);

   QPointer<QDialogButtonBox> editButtons = new QDialogButtonBox(QDialogButtonBox::Ok
                                          | QDialogButtonBox::Cancel);

   QPointer<QFormLayout> presetToEditLayout = new QFormLayout;
   presetToEditLayout -> addRow (QStringLiteral("Coordinate"), coordLine);
   presetToEditLayout -> addRow (QStringLiteral("Abbreviation"), abbrLine);
   presetToEditLayout -> addRow (QStringLiteral("Description"), descrBox);

   QPointer<QVBoxLayout> editPresetMainLayout = new QVBoxLayout;
   editPresetMainLayout -> addLayout (presetToEditLayout);
   editPresetMainLayout -> addWidget (editButtons);

   editPresetDialog -> setWindowTitle (QString(QStringLiteral("Edit: ") + old_abbr));
   editPresetDialog -> setLayout (editPresetMainLayout);

   connect (editButtons, &QDialogButtonBox::rejected, editPresetDialog, &QDialog::reject);
   connect (editButtons, &QDialogButtonBox::accepted, editPresetDialog, &QDialog::accept);

   int modifyDialogResult = editPresetDialog -> exec();

   if (modifyDialogResult == QDialog::Rejected)
      return;

   bool modifyPreset = true;

   if (coordLine -> text().isEmpty()
     || abbrLine -> text().isEmpty()
     || descrBox -> document() -> isEmpty())
   {
      emit errorMessage (QStringLiteral("No changes applied: empty values"));
      modifyPreset = false;
   }
   else if (coordLine -> text() == old_coords
        && abbrLine -> text() == old_abbr
        && descrBox -> toPlainText() == old_description)
   {
      emit errorMessage (QStringLiteral("No changes applied: nothing changed"));
      modifyPreset = false;
   }
   else if (coordLine -> text().size() == GLYPHCODE_LENGTH || coordLine -> text() >= COORDINATE_LENGTH)
   {
      // Verifying this is a valid code to use
      if (coordLine -> text().size() >= COORDINATE_LENGTH && !is_valid_coordinate (coordLine -> text().left (COORDINATE_LENGTH)))
      {
         emit errorMessage (QString(QStringLiteral("Invalid coordinate entered: ") + coordLine -> text()));
         modifyPreset = false;
      }
      else if (coordLine -> text().size() == GLYPHCODE_LENGTH && !is_acceptable_glyph_code (coordLine -> text()))
      {
         emit errorMessage (QString(QStringLiteral("Invalid glyph code entered: ") + coordLine -> text()));
         modifyPreset = false;
      }
      else
      {
         new_code = coordLine -> text();
         new_abbr = abbrLine -> text();
         new_description = descrBox -> toPlainText();
      }

      if (new_code != old_coords)
      {
         if (new_code.size() == GLYPHCODE_LENGTH)
            new_coords = new_code.toUpper();
         else if (new_code.size() == COORDINATE_LENGTH)
         {
            /*
             * Prompt for a portal number since it was not given above
             * with the coordinate
             */
            bool ok;
            new_coords = new_code.toUpper() + QStringLiteral(", portal #");
            int old_portal = old_coords.section ('#', 1, 1).toInt();
            int new_portal = QInputDialog::getInt (this,
                                                   QString (QStringLiteral("Edit portal number for ") + new_code),
                                                   QStringLiteral("Portal Number"), old_portal, 1, PORTAL_MAX, 1, &ok);

            if (ok)
               new_coords += QString::number(new_portal);
            else
               new_coords += QString::number(old_portal);
         }
         else if (coordLine -> text().size() > COORDINATE_LENGTH)
         {
            // Parse the input for the portal number
            bool ok;
            int new_portal = new_code.section ('#', 1, 1).toInt(&ok);
            new_coords = new_code.section (',', 0, 0).toUpper() + QStringLiteral(", portal #") + QString::number(new_portal);
         }
         else
         {
            emit errorMessage (QStringLiteral("No changes applied: bad coordinate code format"));
            modifyPreset = false;
         }
      }
      else
         new_coords = old_coords;
   }

   // Apply changes to preset data
   if (modifyPreset)
   {
       recordToChange.setValue("coordinate", new_coords);
       recordToChange.setValue("abbreviation", new_abbr);
       recordToChange.setValue("description", new_description);
       presetModel -> setRecord (index, recordToChange);
       emit presetChanged();
       emit selectionChange(new_abbr, QStringLiteral("change"), index);
       emit descriptionChange(new_description);
       setStatusTip (new_coords);
   }
}
//------------------------------------------------------------------
bool PresetWidget::isValidIndex (const int index) const
{
   bool result = false;

   if (index >= 0 && index < count())
      result = true;

   return (result);
}
//------------------------------------------------------------------
void PresetWidget::updatePresets()
{
   presetModel -> select();
}
//------------------------------------------------------------------
}  // end of namespace: NMS
