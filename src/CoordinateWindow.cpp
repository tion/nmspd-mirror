//------------------------------------------------------------------
#include "../include/CoordinateWindow.h"
#include "../include/CoordinateDataHelpers.h"
//------------------------------------------------------------------
const int PORTAL_MAX = 16;
const int COORDINATE_LENGTH = 19;
const int GLYPHCODE_LENGTH = 12;
//------------------------------------------------------------------
namespace NMS {
//------------------------------------------------------------------
//////////////////////////////////
// Class: CoordinateInputWidget //
//////////////////////////////////
//------------------------------------------------------------------
CoordinateInputWidget::CoordinateInputWidget (const QString &_coords,
      QWidget *parent)
   : QWidget (parent), 
   coordinateInput (new QLineEdit), 
   portalSelect (new QSpinBox)
{
   QRegularExpression expression (QString (QStringLiteral("0[0-9A-F]{3}:00[0-9A-F]{2}:0[0-9A-F]{3}:0[0-2][0-9A-F]{2}")), 
         QRegularExpression::CaseInsensitiveOption);
   coordValidator = new QRegularExpressionValidator (expression);

   coordinateInput -> setValidator (coordValidator);
   coordinateInput -> setPlaceholderText (QStringLiteral("0FFF:00FF:0FFF:02FF"));
   coordinateInput -> setToolTip (QStringLiteral("Coordinates"));
   coordinateInput -> setCursorMoveStyle (Qt::VisualMoveStyle);
   coordinateInput -> setClearButtonEnabled (true);
   coordinateInput -> setStatusTip (QStringLiteral("Proper coordinate form: 0HHH:00HH:0HHH:0HHH"));

   if (!_coords.isEmpty())
      coordinateInput -> setText (_coords);

   portalSelect -> setToolTip (QStringLiteral("Portal Number"));
   portalSelect -> setRange (1, PORTAL_MAX);
   portalSelect -> setValue (1);

   QPointer<QFormLayout> mainLayout = new QFormLayout;
   mainLayout -> addRow (portalSelect, coordinateInput);

   setLayout (mainLayout);
}
//------------------------------------------------------------------
CoordinateInputWidget::~CoordinateInputWidget()
{
   coordinateInput -> setValidator (nullptr);
   delete coordValidator;
   delete coordinateInput;
   delete portalSelect;
}
//------------------------------------------------------------------
/////////////////////////////////
// Class: GlyphCodeInputWidget //
/////////////////////////////////
//------------------------------------------------------------------
GlyphCodeInputWidget::GlyphCodeInputWidget (const QString &_code,
      QWidget *parent)
   : QWidget (parent), 
   glyphcodeInput (new QLineEdit)
{
   QRegularExpression expression ("[0-9A-F][0-2][0-9A-F]{10}", QRegularExpression::CaseInsensitiveOption);
   glyphValidator = new QRegularExpressionValidator (expression);

   glyphcodeInput -> setMaxLength (GLYPHCODE_LENGTH);
   glyphcodeInput -> setValidator (glyphValidator);
   glyphcodeInput -> setPlaceholderText (QStringLiteral("F00101001001"));
   glyphcodeInput -> setToolTip (QStringLiteral("Glyph Code"));
   glyphcodeInput -> setCursorMoveStyle (Qt::VisualMoveStyle);
   glyphcodeInput -> setCursorPosition (0);
   glyphcodeInput -> setClearButtonEnabled (true);

   if (!_code.isEmpty())
      glyphcodeInput -> setText (_code);

   QPointer<QFormLayout> mainLayout = new QFormLayout;
   mainLayout -> addRow (new QLabel (QStringLiteral("Code sequence:")), glyphcodeInput);

   setLayout (mainLayout);
}
//------------------------------------------------------------------
GlyphCodeInputWidget::~GlyphCodeInputWidget()
{
   glyphcodeInput -> setValidator (nullptr);
   delete glyphValidator;
   delete glyphcodeInput;
}
//------------------------------------------------------------------
/////////////////////////////
// Class: CoordinateWindow //
/////////////////////////////
//------------------------------------------------------------------
CoordinateWindow::CoordinateWindow (const QString &_code,
                                    QWidget *parent)
   : QDialog (parent), 
   tabWidget (new QTabWidget), 
   buttonBox (new QDialogButtonBox)
{
   QPointer<NMS::CoordinateInputWidget> _coords = new NMS::CoordinateInputWidget(_code, this);
   _presets = new NMS::PresetWidget(this);
   QString temp_code = QString();

   if (!_code.isEmpty())
      temp_code = _code.toUpper();

   tabWidget -> addTab (_coords, QStringLiteral("Coordinates"));
   tabWidget -> addTab (new NMS::GlyphCodeInputWidget, QStringLiteral("Glyph Code"));
   tabWidget -> addTab (new NMS::PortalDialerWidget, QStringLiteral("Portal Dial"));
   tabWidget -> addTab (_presets, QStringLiteral("Presets"));

   buttonBox -> addButton (QStringLiteral("Convert"), QDialogButtonBox::AcceptRole);
   buttonBox -> addButton (QStringLiteral("Reset"), QDialogButtonBox::RejectRole);

   QPointer<QVBoxLayout> mainLayout = new QVBoxLayout;
   mainLayout -> addWidget (tabWidget);
   mainLayout -> addWidget (buttonBox);

   setLayout (mainLayout);
   setWindowTitle (QStringLiteral("Portal Decoder"));

   if (!temp_code.isEmpty())
   {
      if (temp_code.size() == COORDINATE_LENGTH)
         setCoordinate (temp_code, int(1));
      else if (temp_code.size() == GLYPHCODE_LENGTH)
         setGlyphs (temp_code);
      else { }
   }

   /*
    * Handle button events
    */
   connect (buttonBox, &QDialogButtonBox::accepted, this, [=]() { convert(); });
   connect (buttonBox, &QDialogButtonBox::rejected, this, &NMS::CoordinateWindow::clear);
   connect (_presets, &NMS::PresetWidget::preset, this, [=](CoordinateData &b)
   {
      /*
       * Provides portal address without verifying coordinate 
       * data. Preset data must ALWAYS be valid.
       */
      setCoordinate (b.getCoordinate(), b.getPortal());
      setGlyphCode (b.getGlyphCode());
      emit saveData (b);
      emit validCode (b.getGlyphCode());
   });

   /*
    * Handle/pass off preset events
    */
   connect (this, &NMS::CoordinateWindow::resetPresetsSignal, _presets, &NMS::PresetWidget::clear);
   connect (this, &NMS::CoordinateWindow::addNewPreset, _presets, &NMS::PresetWidget::addPreset);
   connect (this, &NMS::CoordinateWindow::importPresetsSignal, _presets, &NMS::PresetWidget::fromFile);
   connect (this, &NMS::CoordinateWindow::exportPresetSignal, _presets, &NMS::PresetWidget::toFile);
}
//------------------------------------------------------------------
CoordinateWindow::~CoordinateWindow()
{
   delete _presets;
   delete buttonBox;
   delete tabWidget;
}
//------------------------------------------------------------------
//////////////////
// PUBLIC SLOTS //
//////////////////
//------------------------------------------------------------------
void CoordinateWindow::setPreset()
{
   if (tabWidget -> currentIndex() < 0 
     || tabWidget -> currentIndex() == indexOf (QStringLiteral("preset")))
   {
      emit errorMessage (QStringLiteral("Wrong tab!"));
      return;
   }

   QString presetData = QString();

   if (tabWidget -> currentIndex() == indexOf (QStringLiteral("coordinate")))
   {
      QStringList _tempCoords (getCoordinate());

      // Make sure we're grabbing a valid coordinate
      if (!_tempCoords.isEmpty() && !is_valid_coordinate (_tempCoords.at (0)))
      {
         emit errorMessage (QStringLiteral("No valid coordinate entered"));
         return;
      }

      QString _coord = _tempCoords.at (0).toUpper();
      int _portal = _tempCoords.at (1).toInt();
      presetData = QString (_coord + QStringLiteral (", portal #") + QString::number (_portal));
   }
   else if (tabWidget -> currentIndex() == indexOf (QStringLiteral("glyph")))
   {
      QString _tempGlyph = getGlyph();

      // Make sure we're grabbing a valid glyph code
      if (!is_acceptable_glyph_code (_tempGlyph))
      {
         emit errorMessage (QStringLiteral("No valid glyph code entered"));
         return;
      }

      presetData = _tempGlyph.toUpper();
   }
   else
   {
      QString _tempGlyph = getGlyphs();

      // Make sure we're grabbing a valid glyph code
      if (!is_acceptable_glyph_code (_tempGlyph))
      {
         emit errorMessage (QStringLiteral("No valid glyph code entered"));
         return;
      }

      presetData = _tempGlyph.toUpper();
   }

   QString abbr = QString(QStringLiteral("New Preset for ") + presetData + QStringLiteral(" (edit me)"));
   QString descr = QString(QStringLiteral("A description for ") + presetData + QStringLiteral(" (edit me)"));
   emit addNewPreset(presetData, abbr, descr);
}
//------------------------------------------------------------------
void CoordinateWindow::clear()
{
   QLineEdit *pLine = nullptr;
   QSpinBox *pBox = nullptr;
   QWidget *pWidget = nullptr;
   QList<QSpinBox *> allSpinBoxes;
   QList<QLineEdit *> allLineEdits;
   int page = indexOf (QStringLiteral("coordinate"));

   if (page >= 0 && page < tabWidget -> count())
   {
      pWidget = tabWidget -> widget (page);
      allSpinBoxes = pWidget -> findChildren<QSpinBox *>();

      if (allSpinBoxes.count() > 0)
      {
         for (int j = 0; j < allSpinBoxes.count(); j++)
         {
            if (allSpinBoxes[j] -> metaObject() -> className() == QStringLiteral ("QSpinBox") 
               && allSpinBoxes[j] -> toolTip() == QStringLiteral ("Portal Number"))
            {
               pBox = allSpinBoxes[j];
               pBox -> setValue (1);
            }
         }
      }

      allLineEdits = pWidget -> findChildren<QLineEdit *>();

      if (allLineEdits.count() > 0)
      {
         for (int j = 0; j < allLineEdits.count(); j++)
         {
            if (allLineEdits[j] -> metaObject() -> className() == QStringLiteral ("QLineEdit") 
               && (allLineEdits[j] -> toolTip() == QStringLiteral ("Coordinates")))
            {
               pLine = allLineEdits[j];
               pLine -> clear();
            }
         }
      }
   }

   page = indexOf (QStringLiteral ("glyph"));

   if (page >= 0 && page < tabWidget -> count())
   {
      pWidget = tabWidget -> widget (page);
      allLineEdits = pWidget -> findChildren<QLineEdit *>();

      if (allLineEdits.count() > 0)
      {
         for (int j = 0; j < allLineEdits.count(); j++)
         {
            if (allLineEdits[j] -> metaObject() -> className() == QStringLiteral ("QLineEdit") 
               && allLineEdits[j] -> toolTip() == QStringLiteral ("Glyph Code"))
            {
               pLine = allLineEdits[j];
               pLine -> clear();
               pLine -> setCursorPosition (0);
            }
         }
      }
   }

   setGlyphs (QString());
   emit resetSignal();

   // Pointer clean-up
   if (pBox != nullptr)
      pBox = nullptr;

   if (pLine != nullptr)
      pLine = nullptr;

   if (pWidget != nullptr)
      pWidget = nullptr;

   if (allLineEdits.count() > 0)
      allLineEdits.clear();

   if (allSpinBoxes.count() > 0)
      allSpinBoxes.clear();

   delete pBox;
   delete pLine;
   delete pWidget;
}
//------------------------------------------------------------------
void CoordinateWindow::resetPresets()
{
   emit resetPresetsSignal();
}
//------------------------------------------------------------------
CoordinateData CoordinateWindow::exportData()
{
   CoordinateData result = CoordinateData();
   QStringList temp (getCoordinate());

   if (!temp.at (0).isEmpty())
   {
      result.setCoordinate (temp.at (0));

      if (temp.size() >= 2)
      {
         bool ok;
         result.setPortal (temp.at (1).toInt (&ok));
      }
      else
         result.setPortal (1);
   }

   result.setGlyphCode (getGlyph());

   return (result);
}
//------------------------------------------------------------------
void CoordinateWindow::importData (const CoordinateData &_data)
{
   if (!_data.isEmpty())
   {
      setCoordinate (_data.getCoordinate(), _data.getPortal());
      setGlyphCode (_data.getGlyphCode());
   }
}
//------------------------------------------------------------------
void CoordinateWindow::loadPresets (const QString &_file)
{
   emit importPresetsSignal (_file);
}
//------------------------------------------------------------------
void CoordinateWindow::savePresets (const QString &_file)
{
   emit exportPresetSignal (_file);
}
//------------------------------------------------------------------
void CoordinateWindow::setGlyphs (const QString &_code)
{
   int page = indexOf (QStringLiteral("portal"));

   if (page >= 0 && page < tabWidget -> count())
   {
      QWidget *tempWidget = tabWidget -> widget (page);
      QList<QLineEdit *> allLineEdits = tempWidget -> findChildren<QLineEdit *>();

      if (allLineEdits.count() > 0)
      {
         QLineEdit *pLine = nullptr;

         for (int i = 0; i < allLineEdits.count(); i++)
         {
            if (allLineEdits[i] -> metaObject() -> className() == QStringLiteral ("QLineEdit"))
            {
               pLine = allLineEdits[i];

               if (pLine -> text().toLower() != _code.toLower())
                  pLine -> setText (_code.toUpper());
            }
         }

         if (pLine != nullptr)
            pLine = nullptr;

         delete pLine;
      }

      // Pointer clean-up
      if (tempWidget != nullptr)
         tempWidget = nullptr;

      if (allLineEdits.count() > 0)
         allLineEdits.clear();

      delete tempWidget;
   }
}
//------------------------------------------------------------------
void CoordinateWindow::setGlyphCode (const QString &_code)
{
   QLineEdit *tempLine = nullptr;
   QList<QLineEdit *> allLineEdits;
   QWidget *pWidget = nullptr;
   int page = indexOf (QStringLiteral("glyph"));

   if (page >= 0 && page < tabWidget -> count())
   {
      pWidget = tabWidget -> widget (page);
      allLineEdits = pWidget -> findChildren<QLineEdit *>();

      if (allLineEdits.count() > 0)
      {
         for (int i = 0; i < allLineEdits.count(); i++)
         {
            if (allLineEdits[i] -> metaObject() -> className() == QStringLiteral ("QLineEdit") 
               && allLineEdits[i] -> toolTip() == QStringLiteral ("Glyph Code"))
            {
               tempLine = allLineEdits[i];
               tempLine -> setText (_code);
            }
         }
      }
   }

   // Pointer clean-up
   if (tempLine != nullptr)
      tempLine = nullptr;

   if (pWidget != nullptr)
      pWidget = nullptr;

   if (allLineEdits.count() > 0)
      allLineEdits.clear();

   delete tempLine;
   delete pWidget;
}
//------------------------------------------------------------------
void CoordinateWindow::setCoordinate (const QString &_code, const int _portal)
{
   QLineEdit *tempLine = nullptr;
   QSpinBox *tempBox = nullptr;
   QList<QLineEdit *> allLineEdits;
   QList<QSpinBox *> allSpinBox;
   QWidget *pWidget = nullptr;
   int page = indexOf (QStringLiteral("coordinate"));

   if (page >= 0 && page < tabWidget -> count())
   {
      pWidget = tabWidget -> widget (page);
      allLineEdits = pWidget -> findChildren<QLineEdit *>();

      if (allLineEdits.count() > 1)
      {
         for (int i = 0; i < allLineEdits.count(); i++)
         {
            if (allLineEdits[i] -> metaObject() -> className() == QStringLiteral ("QLineEdit") 
               && allLineEdits[i] -> toolTip() == QStringLiteral ("Coordinates"))
            {
               tempLine = allLineEdits[i];
               tempLine -> setText (_code);
            }
         }
      }

      allSpinBox = pWidget -> findChildren<QSpinBox *>();

      if (allSpinBox.count() > 0)
      {
         for (int i = 0; i < allSpinBox.count(); i++)
         {
            if (allSpinBox[i] -> metaObject() -> className() == QStringLiteral ("QSpinBox")
               && allSpinBox[i] -> toolTip() == QStringLiteral ("Portal Number"))
            {
               tempBox = allSpinBox[i];
               tempBox -> setValue (_portal);
            }
         }
      }
   }

   // Pointer clean-up
   if (tempLine != nullptr)
      tempLine = nullptr;

   if (tempBox != nullptr)
      tempBox = nullptr;

   if (pWidget != nullptr)
      pWidget = nullptr;

   if (allLineEdits.count() > 0)
      allLineEdits.clear();

   if (allSpinBox.count() > 0)
      allSpinBox.clear();

   delete tempLine;
   delete tempBox;
   delete pWidget;
}
//------------------------------------------------------------------
void CoordinateWindow::setData (const QStringList &_code)
{
   if (!_code.isEmpty())
   {
      bool isProperCode = false;

      if (_code.size() == 2 && !_code.at (0).isEmpty())
      {
         isProperCode = is_valid_coordinate (_code.at (0));

         if (isProperCode)
         {
            bool ok;
            setCoordinate (_code.at (0), _code.at (1).toInt (&ok));
         }
      }
      else
      {
         isProperCode = is_valid_glyph_code (_code.at (0));

         if (isProperCode)
            setGlyphCode (_code.at (0));
      }
   }
}
//------------------------------------------------------------------
void CoordinateWindow::showErrorMessage (const QString &_message)
{
   QMessageBox *errorBox = new QMessageBox;
   errorBox -> setText (QStringLiteral("D'oh!"));
   errorBox -> setDetailedText (_message);
   errorBox -> setIcon (QMessageBox::Critical);
   errorBox -> setStandardButtons (QMessageBox::Ok);
   errorBox -> setDefaultButton (QMessageBox::Ok);
   errorBox -> setTextFormat (Qt::RichText);
   errorBox -> setInformativeText (QString (QStringLiteral(
      "The code you are attempting to convert is invalid.") + 
      QStringLiteral("<br><br>") +
      QStringLiteral("Please check your inputs and try again.")));

   errorBox -> exec();
}
//------------------------------------------------------------------
///////////////////
// PRIVATE SLOTS //
///////////////////
//------------------------------------------------------------------
void CoordinateWindow::convert()
{
   if (tabWidget -> currentIndex() == indexOf (QStringLiteral("NMS::CoordinateInputWidget")))
   {
      QStringList _list (getCoordinate());

      if (_list.size() == 2 && !_list.at (0).isEmpty()
        && _list.at (0) != QStringLiteral (":::"))
      {
         QString result = convert_coordinate_to_glyph_code (_list.at (0), _list.at (1).toInt());

         if (!result.isEmpty())
         {
            setGlyphCode (result);
            emit saveData (CoordinateData (result));
            emit validCode (result);
         }
         else
         {
            emit errorMessage (QString (QStringLiteral ("Invalid coordinates. Please fix:") + QString ("\n") + QStringLiteral ("[") + check_coordinate_for_errors(_list.at (0)).join("] [") + QStringLiteral ("]")));
         }
      }
      else
         emit errorMessage (QStringLiteral("Incomplete coordinates"));
   }
   else if (tabWidget -> currentIndex() == indexOf (QStringLiteral("NMS::GlyphCodeInputWidget"))
     || tabWidget -> currentIndex() == indexOf (QStringLiteral("NMS::PortalDialerWidget")))
   {
      QString _temp = QString();

      if (tabWidget -> currentIndex() == indexOf (QStringLiteral("NMS::GlyphCodeInputWidget")))
         _temp = getGlyph();
      else
         _temp = getGlyphs();

      if (_temp.isEmpty() || _temp.size() > GLYPHCODE_LENGTH)
         emit errorMessage (QStringLiteral("Incomplete glyph code"));
      else
      {
         if (is_acceptable_glyph_code(_temp))
         {
            setGlyphCode (_temp);
            QStringList result = convert_glyph_code_to_coordinate (_temp);

            if (result.size() == 2 && !result.at (0).isEmpty())
            {
               setCoordinate (result.at (0), result.at (1).toInt());
            }
            else
               setCoordinate (QString(), int(1));

            emit saveData (CoordinateData (_temp));
            emit validCode (_temp);
         }
         else
         {
            emit errorMessage (QString (QStringLiteral ("Invalid glyph code. Please fix:") + QString ("\n") + QStringLiteral ("[") + check_glyph_code_for_errors (_temp).join ("] [") + QStringLiteral ("]")));
         }
      }
   }
   else if (tabWidget -> currentIndex() == indexOf (QStringLiteral("NMS::PresetWidget")))
   {
      emit errorMessage (QStringLiteral("Choose a different tab"));
   }
   else
   {
      qDebug() << "[CoordinateWindow][convert] Error: invalid tab selected; that shouldn't be possible";
   }
}
//------------------------------------------------------------------
///////////////////////
// PRIVATE FUNCTIONS //
///////////////////////
//------------------------------------------------------------------
int CoordinateWindow::indexOf (const QString &widget_name) const
{
   int index = -1;

   if (widget_name.isEmpty())
      return (index);

   QString findThisWidget;

   if (widget_name.contains (QStringLiteral("NMS::"), Qt::CaseSensitive))
      findThisWidget = widget_name;
   else if (widget_name.toLower() == QStringLiteral ("coordinate"))
      findThisWidget = QStringLiteral("NMS::CoordinateInputWidget");
   else if (widget_name.toLower() == QStringLiteral ("glyph"))
      findThisWidget = QStringLiteral("NMS::GlyphCodeInputWidget");
   else if (widget_name.toLower() == QStringLiteral ("portal"))
      findThisWidget = QStringLiteral("NMS::PortalDialerWidget");
   else if (widget_name.toLower() == QStringLiteral ("preset"))
      findThisWidget = QStringLiteral("NMS::PresetWidget");
   else
   {
      qDebug() << "Invalid widget name:" << widget_name;
      findThisWidget = QString();
      return (index);
   }

   for (int i = 0; i < tabWidget -> count(); i++)
   {
      if (tabWidget -> widget (i) -> metaObject() -> className() == findThisWidget)
      {
         index = i;
         break;
      }
   }

   return (index);
}
//------------------------------------------------------------------
int CoordinateWindow::indexOf (const char *widget_name) const
{
   return (indexOf (QString (widget_name)));
}
//------------------------------------------------------------------
//////////////////////
// PUBLIC FUNCTIONS //
//////////////////////
//------------------------------------------------------------------
QStringList CoordinateWindow::getCoordinate() const
{
   QStringList result = QStringList();
   QLineEdit *tempLine = nullptr;
   QSpinBox *tempBox = nullptr;
   QWidget *pWidget = nullptr;
   QList<QLineEdit *> allLineEdits;
   QList<QSpinBox *> allSpinBox;
   int page = indexOf (QStringLiteral("coordinate"));

   if (page >= 0 && page < tabWidget -> count())
   {
      pWidget = tabWidget -> widget (page);
      allLineEdits = pWidget -> findChildren<QLineEdit *>();

      if (allLineEdits.count() > 1)
      {
         for (int i = 0; i < allLineEdits.count(); i++)
         {
            if (allLineEdits[i] -> metaObject() -> className() == QStringLiteral ("QLineEdit") 
               && allLineEdits[i] -> toolTip() == QStringLiteral ("Coordinates"))
            {
               tempLine = allLineEdits[i];

               if (tempLine -> text().isEmpty()
                 || tempLine -> text() == QString (":::") 
                 || !tempLine -> hasAcceptableInput())
                  result << (QString());
               else
                  result << (tempLine -> text());
            }
         }
      }

      allSpinBox = pWidget -> findChildren<QSpinBox *>();

      if (allSpinBox.count() > 0)
      {
         for (int i = 0; i < allSpinBox.count(); i++)
         {
            if (allSpinBox[i] -> metaObject() -> className() == QStringLiteral ("QSpinBox") 
               && allSpinBox[i] -> toolTip() == QStringLiteral ("Portal Number"))
            {
               tempBox = allSpinBox[i];
               result << (QString::number (tempBox -> value()));
            }
         }
      }
   }

   // Pointer clean-up
   if (tempLine != nullptr)
      tempLine = nullptr;

   if (tempBox != nullptr)
      tempBox = nullptr;

   if (pWidget != nullptr)
      pWidget = nullptr;

   if (allLineEdits.count() > 0)
      allLineEdits.clear();

   if (allSpinBox.count() > 0)
      allSpinBox.clear();

   delete tempLine;
   delete tempBox;
   delete pWidget;

   return (result);
}
//------------------------------------------------------------------
QString CoordinateWindow::getGlyph() const
{
   QString result = QString();
   QLineEdit *tempLine = nullptr;
   QWidget *tempWidget = nullptr;
   QList<QLineEdit *> allLineEdits;
   int page = indexOf (QStringLiteral("glyph"));

   if (page >= 0 && page < tabWidget -> count())
   {
      tempWidget = tabWidget -> widget (page);
      allLineEdits = tempWidget -> findChildren<QLineEdit *>();

      if (allLineEdits.count() > 0)
      {
         for (int i = 0; i < allLineEdits.count(); i++)
         {
            if (allLineEdits[i] -> metaObject() -> className() == QStringLiteral ("QLineEdit") 
               && allLineEdits[i] -> toolTip() == QStringLiteral ("Glyph Code"))
            {
               tempLine = allLineEdits[i];
               result.append (tempLine -> text());
            }
         }
      }
   }

   // Pointer clean-up
   if (tempLine != nullptr)
      tempLine = nullptr;

   if (tempWidget != nullptr)
      tempWidget = nullptr;

   if (allLineEdits.count() > 0)
      allLineEdits.clear();

   delete tempLine;
   delete tempWidget;

   return (result);
}
//------------------------------------------------------------------
QString CoordinateWindow::getGlyphs() const
{
   QString result = QString();
   int page = indexOf (QStringLiteral("portal"));

   if (page >= 0 && page < tabWidget -> count())
   {
      QWidget *tempWidget = tabWidget -> widget (page);
      QList<QLineEdit *> allLineEdits = tempWidget -> findChildren<QLineEdit *>();

      if (allLineEdits.count() > 0)
      {
         QLineEdit *pLine = nullptr;

         for (int i = 0; i < allLineEdits.count(); i++)
         {
            if (allLineEdits[i] -> metaObject() -> className() == QStringLiteral ("QLineEdit"))
            {
               pLine = allLineEdits[i];

               if (!pLine -> text().isEmpty())
                  result = pLine -> text();
            }
         }

         if (pLine != nullptr)
            pLine = nullptr;

         delete pLine;
      }

      // Pointer clean-up
      if (tempWidget != nullptr)
         tempWidget = nullptr;

      if (allLineEdits.count() > 0)
         allLineEdits.clear();

      delete tempWidget;
   }

   return (result);
}
//------------------------------------------------------------------
}  // end of namespace: NMS
