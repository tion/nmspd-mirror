//------------------------------------------------------------------
#include <QRegularExpressionMatch>
#include <sstream>               // std::stringstream
#include <iomanip>               // std::hex, std::dec

#include "../include/CoordinateDataHelpers.h"
//------------------------------------------------------------------
int convert_hex_to_integer (const QString &_code)
{
   int result = -1;
   bool ok;
   std::stringstream readString;
   QString convertTheString = QString();

   readString << std::dec << _code.toUtf8().constData();
   std::string tempString (readString.str());
   convertTheString = QString::fromStdString (tempString);
   result = convertTheString.toInt (&ok, 16);

   return (result);
}
//------------------------------------------------------------------
QString convert_integer_to_hex (const int number)
{
   QString result = QString();
   std::stringstream outputString;

   outputString << std::hex << number;
   std::string tempString (outputString.str());
   result = QString::fromStdString (tempString);

   return (result);
}
//------------------------------------------------------------------
QString pad_with_zeros (const QString &code, const int padding)
{
   QString result = code;

   if (padding > 0 && padding <= 4)
   {
      for (int i = code.size(); i < padding; i++)
         result.insert (0, '0');
   }

   return (result);
}
//------------------------------------------------------------------
bool is_valid_portal (const int _portal)
{
   bool result = false;

   if (_portal >= 1 && _portal <= 16)
      result = true;

   return (result);
}
//------------------------------------------------------------------
bool is_valid_coordinate (const QString &_code)
{
   bool result = false;

   if (check_coordinate_for_errors (_code).isEmpty() == true)
      result = true;

   return (result);
}
//------------------------------------------------------------------
bool is_acceptable_coordinate (const QString &_code)
{
   bool result = false;

   QString tempCode = _code.toUpper();
   QRegularExpression coordinateInputMask ("[[0-9A-F]{4}:]{3}[0-9A-F]");
   QRegularExpressionMatch validCoords = coordinateInputMask.match (tempCode);

   if (validCoords.hasMatch() == true)
      result = true;

   return (result);
}
//------------------------------------------------------------------
QStringList check_coordinate_for_errors (const QString &_code)
{
   QStringList errorList = QStringList();
   QString tempCode = _code.toUpper();

   // Check that tempCode satisfies input mask:
   //    HHHH:HHHH:HHHH:HHHH
   if (is_acceptable_coordinate(tempCode) == true
         || (tempCode.size() > 0 && tempCode.size() <= 19))
   {
      QString coord_segment = QString();
      int dec_of_segment = 0;
      const QRegularExpression validHex ("[0-9A-F]{4}");
      QRegularExpressionMatch segmentHex;

      /*
       * Check each segment of the coordinate
       *
       * Valid coordinates:
       *  => 0001:0001:0001:0001 .. 0FFF:00FF:0FFF:02FF
       *  => 1:1:1:1 .. 4095:255:4095:767
       */
      for (int i = 0; i < 4; i++)
      {
         coord_segment = QString (tempCode.section (':', i, i));
         segmentHex = validHex.match (coord_segment);

         if (segmentHex.hasMatch() == true)
         {
            dec_of_segment = convert_hex_to_integer (coord_segment);

            if (i == 1)
            {
               // Y-coordinate
               if (dec_of_segment < 1 || dec_of_segment > 255)
                  errorList.append (pad_with_zeros (coord_segment, 4));
            }
            else if (i == 3)
            {
               // Star Index/Class
               if (dec_of_segment < 1 || dec_of_segment > 767)
                  errorList.append (pad_with_zeros (coord_segment, 4));
            }
            else
            {
               // X-coordinate and Z-coordinate
               if (dec_of_segment < 1 || dec_of_segment > 4095)
                  errorList.append (pad_with_zeros (coord_segment, 4));
            }
         }
         else
            errorList.append (pad_with_zeros (coord_segment, 4));
      }
   }
   else
      errorList.append (tempCode);

   return (errorList);
}
//------------------------------------------------------------------
bool is_valid_glyph_code (const QString &_code)
{
   bool result = false;

   if (check_glyph_code_for_errors (_code).isEmpty() == true)
      result = true;

   return (result);
}
//------------------------------------------------------------------
bool is_acceptable_glyph_code (const QString &_code)
{
   bool result = false;

   const QString testCode = _code.toUpper();
   const QRegularExpression glyphCodeInputMask ("[0-9A-F]{12}");
   QRegularExpressionMatch validHex = glyphCodeInputMask.match (testCode);

   if (validHex.hasMatch() == true)
      result = true;

   return (result);
}
//------------------------------------------------------------------
QStringList check_glyph_code_for_errors (const QString &_code)
{
   QStringList errorList = QStringList();
   QString tempCode = _code.toUpper();

   // Check that tempCode satisfies input mask:
   //  HHHHHHHHHHHH
   if (is_acceptable_glyph_code (_code) == true
         || tempCode.size() == 12)
   {
      QString temp = QString (tempCode.at (1)) + QString (tempCode.at (2)) + QString (tempCode.at (3));
      const int dec_of_starclass = convert_hex_to_integer (temp);

      temp = QString (tempCode.at (4)) + QString (tempCode.at (5));
      const int dec_of_y_axis = convert_hex_to_integer (temp);

      temp = QString (tempCode.at (6)) + QString (tempCode.at (7)) + QString (tempCode.at (8));
      const int dec_of_z_axis = convert_hex_to_integer (temp);

      temp = QString (tempCode.at (9)) + QString (tempCode.at (10)) + QString (tempCode.at (11));
      const int dec_of_x_axis = convert_hex_to_integer (temp);

      const QRegularExpression validCharacter ("[0-9A-F]");
      const QRegularExpression segment ("[0-9A-F]{3}");
      const QRegularExpression segmentY ("[0-9A-F]{2}");
      QRegularExpressionMatch validPortal = validCharacter.match (tempCode.at (0));
      QRegularExpressionMatch segmentHex;

      /*
       * Portal number: 1-digit
       * Min: 0x1 (1)
       * Max: 0xF (16)
       */
      if (validPortal.hasMatch() == false)
         errorList.append (QString (tempCode.at (0)));

      /*
       * Star Index/Class: 3-digits
       * Min: 0x001 (1)
       * Max: 0x2FF (767)
       */
      temp = QString (tempCode.at (1)) + QString (tempCode.at (2)) + QString (tempCode.at (3));
      segmentHex = segment.match (temp);

      if (segmentHex.hasMatch() == true)
      {
         if (dec_of_starclass < 1 || dec_of_starclass > 767)
            errorList.append (pad_with_zeros (convert_integer_to_hex (dec_of_starclass), 3));
      }
      else
         errorList.append (temp);

      /*
       * Y-coordinate: 2-digits
       * Min: 0x01 (1)
       * Max: 0x82 (mod 130)
       */
      temp = QString (tempCode.at (4)) + QString (tempCode.at (5));
      segmentHex = segmentY.match (temp);

      if (segmentHex.hasMatch() == true)
      {
         if ( (dec_of_y_axis % 130) == 129
               || dec_of_y_axis < 1)
         {
            errorList.append (pad_with_zeros (convert_integer_to_hex (dec_of_y_axis), 2));
         }
      }
      else
         errorList.append (temp);

      /*
       * Z-coordinate: 3-digits
       * Min: 0x001 (1)
       * Max: 0xFFF (2048)
       */
      temp = QString (tempCode.at (6)) + QString (tempCode.at (7)) + QString (tempCode.at (8));
      segmentHex = segment.match (temp);

      if (segmentHex.hasMatch() == true)
      {
         if ( (dec_of_z_axis % 2050) == 2049
               || dec_of_z_axis < 1)
         {
            errorList.append (pad_with_zeros (convert_integer_to_hex (dec_of_z_axis), 3));
         }
      }
      else
         errorList.append (temp);

      /*
       * X-coordinate: 3-digits
       * Min: 0x001 (1)
       * Max: 0xFFF (2048)
       */
      temp = QString (tempCode.at (9)) + QString (tempCode.at (10)) + QString (tempCode.at (11));
      segmentHex = segment.match (temp);

      if (segmentHex.hasMatch() == true)
      {
         if ( (dec_of_x_axis % 2050) == 2049
               || dec_of_x_axis < 1)
         {
            errorList.append (pad_with_zeros (convert_integer_to_hex (dec_of_x_axis), 3));
         }
      }
      else
         errorList.append (temp);
   }
   else
   {
      // The entire code is invalid
      errorList.append (tempCode);
   }

   return (errorList);
}
//------------------------------------------------------------------
QString convert_coordinate_to_glyph_code (const QString &_coordinate, const int _portal)
{
   QString glyphcode_result = QString();
   QStringList errorTrap = check_coordinate_for_errors (_coordinate);

   // If errors, do not attempt conversion
   if (errorTrap.isEmpty() == false)
      return (glyphcode_result);

   const QString coords = _coordinate.toUpper();
   QString temp_result = QString();

   // Parse through the colons of the coordinates
   QString hex_of_x_axis = coords.section (':', 0, 0);
   QString hex_of_y_axis = coords.section (':', 1, 1);
   QString hex_of_z_axis = coords.section (':', 2, 2);
   QString hex_of_starclass = coords.section (':', 3, 3);

   int dec_of_x_axis = convert_hex_to_integer (hex_of_x_axis);
   int dec_of_y_axis = convert_hex_to_integer (hex_of_y_axis);
   int dec_of_z_axis = convert_hex_to_integer (hex_of_z_axis);
   int dec_of_starclass = convert_hex_to_integer (hex_of_starclass);
   int temp = 0;

   /*
    * Glyph of star index/class
    * The last 3 digits of the star index -> x:y:z:0SSS
    *
    * The zero is usually reserved for the portal number.
    * Using the entire star index/class assumes portal
    * number 1. So, if wanting a specific portal number,
    * use:
    *
    *  => ((portal number - 1) * (4096))
    *       + (decimal of star index/class)
    */
   temp = (((_portal - 1) * 4096) + dec_of_starclass);
   temp_result = convert_integer_to_hex (temp).toUpper();
   const QString starIndex_result = pad_with_zeros (temp_result, 4);

   /*
    * Glyph of y-axis of star system coordinates
    *
    *  => (decimal of y) + 129
    *  => y + (0x81)
    *
    * Y-coordinate must be 2 characters and no greater
    * than 0xFF (255).
    */
   temp = (dec_of_y_axis + 129);

   // Remove leading digit
   if (temp > 255)
      temp -= 256;

   temp_result = convert_integer_to_hex (temp).toUpper();
   const QString y_axis_result = pad_with_zeros (temp_result, 2);

   /*
    * Glyph of z-axis of star system coordinates
    *
    *  => (decimal of z) + 2049
    *  => z + (0x801)
    *
    * Z-coordinate must be 3 characters, and no greater
    * than 0xFFF (4095)
    */
   temp = (dec_of_z_axis + 2049);

   // Remove leading digit
   if (temp > 4095)
      temp -= 4096;

   temp_result = convert_integer_to_hex (temp).toUpper();
   const QString z_axis_result = pad_with_zeros (temp_result, 3);

   /*
    * Glyph of x-axis of star system coordinates
    *
    *  => (decimal of x) + 2049
    *  => x + (0x801)
    *
    * X-coordinate must be 3 characters, and no greater
    * than 0xFFF (4095)
    */
   temp = dec_of_x_axis + 2049;

   // Remove leading digit
   if (temp > 4095)
      temp -= 4096;

   temp_result = convert_integer_to_hex (temp).toUpper();
   const QString x_axis_result = pad_with_zeros (temp_result, 3);

   /*
    * Finalize the glyph code
    */
   glyphcode_result = QString (starIndex_result
                               + y_axis_result
                               + z_axis_result
                               + x_axis_result);

   return (glyphcode_result);
}
//------------------------------------------------------------------
QStringList convert_glyph_code_to_coordinate (const QString &_code)
{
   QStringList coordinate_result = QStringList();
   QStringList errorTrap = check_glyph_code_for_errors (_code);

   // If errors, do not attempt conversion
   if (errorTrap.isEmpty() == false)
      return (coordinate_result);

   QString temp = QString();
   QString wCode = QString();
   QStringList singleGlyph = _code.split ("");
   int temp_value, offset = 0, temp_convert;

   // Iterate over the "strings" in singleGlyph, each of
   // which is a digit of the glyph code
   for (int i = 0; i < singleGlyph.size(); i++)
      wCode += singleGlyph.at (i).toLocal8Bit().constData();

   /*
    * Star Index/Class is taken directly from the glyph code.
    * Add zero-padding to fill up to 4 digits.
    */
   temp = QString (QString (wCode[1]) + QString (wCode[2]) + QString (wCode[3])).toUpper();
   QString hex_of_starclass = pad_with_zeros (temp, 4);

   /*
    * Y-coordinate
    * Add zero-padding to fill up to 2 digits.
    */
   temp = QString (QString (wCode[4]) + QString (wCode[5]));
   temp_convert = convert_hex_to_integer (temp);

   // Add leading digit if glyph code required truncate.
   if (temp_convert < 129)
      offset = 256;
   else
      offset = 0;

   temp_value = (temp_convert + offset) - 129;
   temp = convert_integer_to_hex (temp_value).toUpper();
   QString hex_of_y_axis = pad_with_zeros (temp, 4);

   /*
    * Z-coordinate
    * Add zero-padding to fill up to 4 digits.
    */
   temp = QString (QString (wCode[6]) + QString (wCode[7]) + QString (wCode[8]));
   temp_convert = convert_hex_to_integer (temp);

   // Add leading digit if glyph code required truncate
   if (temp_convert < 2049)
      offset = 4096;
   else
      offset = 0;

   temp_value = (temp_convert + offset) - 2049;
   temp = convert_integer_to_hex (temp_value).toUpper();
   QString hex_of_z_axis = pad_with_zeros (temp, 4);

   /*
    * X-coordinate
    * Add zero padding to fill up to 4 digits.
    */
   temp = QString (wCode[9]) + QString (wCode[10]) + QString (wCode[11]);
   temp_convert = convert_hex_to_integer (temp);

   // Add leading digit if glyph code required truncate
   if (temp_convert < 2049)
      offset = 4096;
   else
      offset = 0;

   temp_value = (temp_convert + offset) - 2049;
   temp = convert_integer_to_hex (temp_value).toUpper();
   QString hex_of_x_axis = pad_with_zeros (temp, 4);

   /*
    * Finalize the coordinates
    */
   coordinate_result.append (QString (hex_of_x_axis + ":"
                                      + hex_of_y_axis + ":"
                                      + hex_of_z_axis + ":"
                                      + hex_of_starclass));
   coordinate_result.append (QString::number (convert_hex_to_integer (QString (wCode[0])) + 1));

   return (coordinate_result);
}
//------------------------------------------------------------------
