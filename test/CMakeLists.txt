add_executable(coords-test qtest_coordinatedata.cpp)
target_link_libraries(coords-test 
   coords
   Qt5::Test)
add_test(NAME QTest-CoordinateObject COMMAND coords-test)
