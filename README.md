# qportals
A portal decoder for the game No Man's Sky&trade;. 
Converts portal addresses into coordinates and 
vice versa.

![preview](https://i.imgur.com/A7m9Wd4.png)

## Getting Started
### Prerequisites
* Qt5 + headers >= 5.6
  * Modules: Core, Gui, Widgets, Sql (sqlite3), Tests
* C++11 compatible compiler, such as GCC or clang
* CMake >= 3.7.2
* Doxygen >= 1.8.8, for api documentation

### Installing

<pre><code>cd qportals
mkdir build
cd build
cmake ..
make</code></pre>

As an example, if using ninja generator:
<pre><code>cmake -G Ninja ..
ninja</code></pre>

Other build options available:
<pre><code># Build the GUI application. Default is ON.
WITH_GUI=ON|OFF</code></pre>

<pre><code># Build the CLI application. Default is ON.
WITH_CLI=ON|OFF</code></pre>

<pre><code># Build the unit tests. Default is OFF.
WITH_TESTS=ON|OFF</code></pre>

<pre><code># Build api documentation with doxygen. Default is OFF.
WITH_DOCS=ON|OFF</code></pre>

<pre><code># Debug: Build the application with some address sanitizer options enabled.
# Note: This option IS NOT meant to be used together with valgrind.
# Default is OFF.
WITH_ASAN=ON|OFF</code></pre>

Example of building the GUI, unit tests, and documentation with 
the ninja generator:
<pre><code>cd qportals
mkdir build
cd build
cmake -G Ninja -DWITH_CLI=OFF -DWITH_TESTS=ON -DWITH_DOCS=ON ..
ninja</code></pre>

## Running the tests
Assuming tests were built during the above step:
<pre><code>cd build
ctest -j0</code></pre>

There is one test executable, <code>coords-test</code>, which 
tests several edge cases for coordinate and glyph code inputs. 
A successful test will have output similar to the following:
<pre><code>Test project /home/tion/qt/qportals/build
    Start 1: QTest-CoordinateObject
1/1 Test #1: QTest-CoordinateObject ...........   Passed    0.02 sec

<span style="color:green">100% tests passed</span>, 0 tests failed out of 1

Total Test time (real) =   0.02 sec</code></pre>

or if run on its own <code>test/coords-test</code>:
<pre><code>********* Start testing of coordinateDataTest *********
Config: Using QtTest library 5.13.0, Qt 5.13.0 (x86_64-little_endian-lp64 shared (dynamic) release build; by GCC 9.1.0)
PASS   : coordinateDataTest::initTestCase()
PASS   : coordinateDataTest::instantiation()
PASS   : coordinateDataTest::variousCoordinates()
PASS   : coordinateDataTest::variousGlyphcodes()
PASS   : coordinateDataTest::invalidCharacters()
PASS   : coordinateDataTest::cleanupTestCase()
Totals: 6 passed, 0 failed, 0 skipped, 0 blacklisted, 6ms
********* Finished testing of coordinateDataTest *********</code></pre>

## Versioning
All releases on and after 0.7 use [SemVer](https://semver.org/) for 
versioning. For the versions available, see the tags on 
this repository.

## Authors
* [tion](https://codeberg.org/tion) (and [here](https://gitlab.com/tion/))

## License
This project is licensed under the GNU General Public 
License 3.0 - see the license file for 
details.

## Acknowledgements
* [The Pilgrim Star Path](https://pahefu.github.io/pilgrimstarpath/)
* [Portal Decoder App](https://nmsportals.github.io/)
* [The wizards on /r/NMSPortals](https://www.reddit.com/r/NMSCoordinateExchange/wiki/tutorials/convert_portal_coordinates)
