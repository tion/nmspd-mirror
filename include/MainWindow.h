//------------------------------------------------------------------
#ifndef __NMS_MAIN_WINDOW_H__
#define __NMS_MAIN_WINDOW_H__
//------------------------------------------------------------------
#include "CoordinateWindow.h"
#include "PortalAddressWindow.h"
//------------------------------------------------------------------
/** \addtogroup Main
 *  \brief Main widget for the application
 *  @{
 */

/** \class CoordinateMainWindow
 *
 *  \brief The main window to handle coordinate and 
 *  glyph code conversion.
 * 
 *  \details This class consists of 2 widgets:
 *
 *       1) NMS::CoordinateWindow
 *       2) NMS::PortalAddressWidget
 *
 *  #1 is the central widget. As changes occur, and conversions are
 *  requested, they will generally be reflected on #2 automatically. 
 *  The exception to this is with the presets tab within #1. Changes 
 *  to presets do not display automatically. That is, a modified 
 *  preset must be re-selected manually for the new changes to show
 *  up on #2.
 *
 *  #2 is a DockWidget, usually anchored at the top or bottom of the 
 *  central widget.
 *
 *  To create a CoordinateMainWindow object:
 * 
 *       CoordinateMainWindow example (CoordinateData (("0123:0045:0678:00af", 3)));
 *       example.show();
 *
 *  If created on the heap, the CoordinateMainWindow object must be 
 *  freed within the scope it was initialized.
 *
 *  \see NMS::CoordinateWindow NMS::PortalAddressWidget
 */
class CoordinateMainWindow : public QMainWindow
{
   Q_OBJECT

   public:
      /** \details Default constructor.
       *
       *  Initializes with a coordinate value.
       *
       *  \param[in] _code A valid CoordinateData object
       */
      explicit CoordinateMainWindow(const CoordinateData &_code = CoordinateData());
      /** \details Default destructor. Handles clean up of 
       *  UI elements.
       */
      ~CoordinateMainWindow();

   signals:

   public slots:
      /** \details Toggles visibility of portal address window. */
      void showPortalWindow();
      /** \details Resets all widgets to empty state */
      void clear();
      /** \details Sets coordinate values
       *
       *  \param[in] _data A valid CoordinateData object
       */
      void importData(const CoordinateData &_data);
      /** \details Sets coordinate and portal data.
       *
       *  \param[in] _coord A valid coordinate to set
       *  \param[in] _portal A valid portal to set
       */
      void setCoordinates(const QString &_coord, const int _portal);
      /** \details Sets glyph code.
       *
       *  \param[in] _code A valid glyph code
       */
      void setGlyphCode(const QString &_code);

   private:
      QAction *exitAction;
      QAction *aboutAction;
      QAction *aboutQtAction;
      QAction *importAction;
      QAction *exportAction;
      QAction *freshStartAction;
      QAction *preferenceMenuAction;
      QAction *showPortalWindowAction;
      QAction *showPresetWidgetAction;

      QMenu *fileMenu;
      QMenu *helpMenu;

      NMS::CoordinateWindow *main_screen;
      NMS::PortalAddressWidget *address_widget;

   private slots:
      /** \details Shows a QMessageBox with information 
       *  about this program, such as version number 
       *  and license.
       */
      void about();
      /** \details Opens a QDialog to select/edit application 
       *  preferences found in the config file.
       */
      void openPreferences();
};
//------------------------------------------------------------------
/** @} End of doxygen group Main */
//------------------------------------------------------------------
#endif   // __NMS_MAIN_WINDOW_H__
//------------------------------------------------------------------
