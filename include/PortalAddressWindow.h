//------------------------------------------------------------------
#ifndef __NMS_PORTAL_ADDRESS_H__
#define __NMS_PORTAL_ADDRESS_H__
//------------------------------------------------------------------
#include <QtCore/QString>
#include <QtCore/QVector>
#include <QtWidgets/QToolButton>
//------------------------------------------------------------------
namespace NMS {
//------------------------------------------------------------------
/** \addtogroup NMS
 *  \brief Coordinate widgets.
 *  \details Collection of convenience widgets to display and 
 *  manipulate coordinate data.
 *  @{
 */

/** \class PortalAddressWidget
 *
 *  \brief Displays a portal address.
 *
 *  \details Translates a glyph code, a 12-digit 
 *  hexadecimal sequence, into a portal address made up of glyph 
 *  symbols for each digit.
 *
 *  To create a PortalAddressWidget:
 *
 *       // Empty glyph code
 *       PortalAddressWidget example;
 *
 *       // Show glyph code: 200d01345678
 *       example.setGlyphCode ("200d01345678");
 *       example.show();
 *
 *  If created on the heap, the PortalAddressWidget object must be 
 *  freed within the scope it is initialized.
 *
 *  \see is_acceptable_glyph_code
 */
class PortalAddressWidget : public QWidget
{
   Q_OBJECT

   public:
      /** \details Default constructor.
       *
       *  Initializes with the provided _code.
       *
       *  \param[in] _code A valid glyph code to display 
       *  the glyphs for
       *  \param[in] parent The parent widget to draw onto
       */
      explicit PortalAddressWidget(const QString &_code = QString(), QWidget *parent = 0);
      /** \details Default destructor */
      ~PortalAddressWidget();

   public slots:
      /** \details Sets the glyph code to display.
       *
       *  \param[in] _code A valid glyph code
       *
       *  \warning There is no error-checking that _code 
       *  is in fact valid
       */
      void setGlyphCode(const QString &_code);
      /** \details Resets the glyph code to empty state */
      void clear();

   private:
      QVector<QToolButton *> portal_address;
};
//------------------------------------------------------------------
}  // end of namespace: NMS
/** @} End of doxygen group NMS */
//------------------------------------------------------------------
#endif   // __NMS_PORTAL_ADDRESS_H__
//------------------------------------------------------------------