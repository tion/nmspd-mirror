//------------------------------------------------------------------
#ifndef __NMS_COORDINATE_HELPERS_H__
#define __NMS_COORDINATE_HELPERS_H__
//------------------------------------------------------------------
#include <QtCore>
//------------------------------------------------------------------
/** \addtogroup Helpers
 *  \brief Helper class and functions for manipulating coordinates.
 *  \details These are helper objects and functions for converting 
 *  coordinates to glyph codes and glyph codes to coordinates.
 *
 *  CoordinateData is a wrapper class for 
 *  interacting with the coordinate helper functions. All operations 
 *  performed by the class can be done manually. The benefit to using 
 *  the class is that it requires less repeated conversions to obtain 
 *  extra information, as this data is stored within the class. Using 
 *  the class is overkill for one-shot conversions, and is best 
 *  suited for uses that require repeated conversions over time or 
 *  preserving many coordinates.
 *
 *  For example:
 *
 *       // With CoordinateData
 *       CoordinateData a("0123:0045:0678:0199", 6);
 *       a.convertToGlyph();
 *       std::cout << a;
 *
 *       // Manually
 *       std::cout << "Glyph code: ";
 *       std::cout << convert_coordinate_to_glyph_code("0123:0045:0678:0199", 6).toStdString();
 *
 *  @{
 */

/** \fn convert_hex_to_integer(const QString &_code)
 *
 *  \details Converts the hexadecimal value of _code into a 
 *  decimal value.
 *
 *  \param[in] _code A valid hexadecimal value
 *
 *  \returns The decimal equivalent of _code
 */
int convert_hex_to_integer(const QString &_code);
/** \fn convert_integer_to_hex(const int number)
 *
 *  \details Converts the decimal value of number into a 
 *  hexadecimal value.
 *
 *  \param[in] number A valid decimal value
 *
 *  \returns The hexadecimal equivalent of number
 */
QString convert_integer_to_hex(const int number);
/** \fn pad_with_zeros(const QString &code, const int padding)
 * 
 *  \details Prepends zeros to a string.
 * 
 *  \param[in] code A valid, non-empty QString
 *  \param[in] padding A valid, non-negative number 
 *  between 1 and 4
 *
 *  \returns The value of code pre-pended with zeros to fill its size
 *  to the padding parameter
 */
QString pad_with_zeros(const QString &code, const int padding);

/** \fn bool is_valid_coordinate(const QString &_code)
 *
 *  \details Determines if the provided _code is a valid coordinate.
 * 
 *  \param[in] _code A valid QString to check
 * 
 *  \returns True, if _code is a valid coordinate
 */
bool is_valid_coordinate(const QString &_code);
/** \fn is_acceptable_coordinate(const QString &_code)
 *
 *  \details Determines if the provided _code is a properly formed 
 *  coordinate, which is a 16-digit hexadecimal sequence separated 
 *  by a colon (:) every 4 digits.
 *
 *       HHHH:HHHH:HHHH:HHHH
 *
 *  \param[in] _code A valid QString to verify is a coordinate
 *
 *  \returns True, if _code has proper coordinate address form
 */
bool is_acceptable_coordinate(const QString &_code);
/** \fn check_coordinate_for_errors(const QString &_code)
 *
 *  \details Determines if the provided _code is a valid coordinate. 
 *  A valid coordinate is 16 hex characters long, with 3 semicolons to
 *  separate the different axis (x,y,z,star class). Any errors in 
 *  _code are stored in a QStringList, which is the return value.
 * 
 *  \param[in] _code A valid QString to check
 * 
 *  \returns A QStringList holding the invalid segments found
 *  within the provided _code.
 */
QStringList check_coordinate_for_errors(const QString &_code);
/** \fn is_valid_glyph_code(const QString &_code)
 *
 *  \details Determines if the provided _code is a valid glyph code.
 *
 *  \param[in] _code A glyph code to check
 * 
 *  \returns True, if _code is a valid glyph code
 */
bool is_valid_glyph_code(const QString &_code);
/** \fn is_acceptable_glyph_code(const QString &_code)
 *
 *  \details Determines if the provided _code is a properly formed 
 *  glyph code, which is a 12-digit hexadecimal sequence.
 *
 *       HHHHHHHHHHHH
 *
 *  \param[in] _code A valid QString to verify is a glyph code
 *
 *  \returns True, if _code has proper glyph code form
 */
bool is_acceptable_glyph_code(const QString &_code);
/** \fn check_glyph_code_for_errors(const QString &_code)
 *
 *  \details Determines if the provided _code is a valid glyph code.
 *
 *  A valid glyph code is 12 hex characters long. Must also be
 *  between a lower and upper limit:
 *  000182802802 .. F2FF80800800.
 *
 *  \param[in] _code A glyph code to check
 * 
 *  \returns A QStringList holding the invalid segments found
 *  within the provided _code.
 */
QStringList check_glyph_code_for_errors(const QString &_code);
/** \fn convert_coordinate_to_glyph_code(const QString &_coordinate, const int _portal)
 *
 *  \details Converts the provided _coordinate and _portal into 
 *  its corresponding glyph code.
 *
 *  \param[in] _coordinate A valid coordinate value to convert
 *  \param[in] _portal A valid portal number
 *
 *  \returns The glyph code sequence for _coordinate. An empty
 *  value signifies a failed conversion.
 */
QString convert_coordinate_to_glyph_code(const QString &_coordinate, const int _portal = 1);
/** \fn convert_glyph_code_to_coordinate(const QString &_code)
 *
 *  \details Converts the provided _code into its corresponding
 *  coordinate and portal.
 *
 *  \param[in] _code A valid glyph code to convert
 *
 *  \returns QStringList containing the coordinate and portal
 *  number for _code. An empty list signifies
 *  a failed conversion.
 */
QStringList convert_glyph_code_to_coordinate(const QString &_code);
/** \fn is_valid_portal(const int _portal)
 *
 *  \details Determines if provided _portal is a valid portal 
 *  number. Valid numbers are between 1 and 16.
 *
 *  \returns True, if _portal is valid
 */
bool is_valid_portal(const int _portal);
//------------------------------------------------------------------
/** @} End of doxygen group Helpers */
//------------------------------------------------------------------
#endif   // __NMS_COORDINATE_HELPERS_H__
//------------------------------------------------------------------
