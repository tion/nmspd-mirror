//------------------------------------------------------------------
#ifndef __NMS_PRESET_WINDOW_H__
#define __NMS_PRESET_WINDOW_H__
//------------------------------------------------------------------
#include <QSettings>
#include <QtWidgets/QWidget>
#include <QSqlDatabase>
#include <QSqlTableModel>

#include "CoordinateData.h"
//------------------------------------------------------------------
namespace NMS {
//------------------------------------------------------------------
/** \addtogroup NMS
 *  \brief Coordinate widgets.
 *  \details Collection of convenience widgets to display and 
 *  manipulate coordinate data.
 *  @{
 */

/** \class PresetWidget
 * 
 *  \brief Widget for the selection and manipulation of preset 
 *  coordinate data.
 * 
 *  \details This class uses an SQLite database to save and 
 *  manipulate presets. All SQL records are structured as follows:
 *
 *       FIELD : VALUE (data type)
 *       ------------------------
 *       id : primary key (int)
 *       coordinate : text (string)
 *       abbreviation : text (string)
 *       description : text (string)
 *
 *  The ID field corresponds to the record number and is 
 *  generally calculated automatically during queries.
 *
 *  The coordinate field can be either a coordinate + portal number, 
 *  or a glyph code. One or the other, but not both. The format for 
 *  coordinate + portal number is expected to be 
 *  the same as CoordinateData::toString, which is of the form: 
 *
 *       "COORDINATE_ADDRESS, portal #PORTAL_NUMBER"
 *
 *  As an example for the coordinate field:
 *
 *       "0118:001a:03c6:0203, portal #16" // VALID
 *       "f1227c210dc6"    // VALID
 *       "0123456789ab 0f21:0032:0785:0012, portal #8"   // INVALID
 *
 *  The abbreviation field can be anything, but is intended to 
 *  quickly identify a preset, such as: "GHUB-MyBase@Diplos". Right 
 *  now there is no length restriction but this value should 
 *  be kept short.
 *
 *  The description field can also be anything really, but should 
 *  be more descriptive than the abbreviation. For example:
 *
 *       "My base in the Galactic HUB next to a diplo pond on Balarius IX"
 *
 *  When creating PresetWidget on the heap, the object must 
 *  be removed/deallocated within the scope it is instantiated. A 
 *  simple 'delete' should suffice.
 *
 *       void awesomeFunction() {
 *          NMS::PresetWidget *obj1 = new NMS::PresetWidget;
 *          // Do stuff with obj1
 *
 *          // Done with obj1, return it to the heap
 *          delete obj1;
 *       }
 *
 *  PresetWidget objects created on the stack do not require 
 *  special handling; they are automatically de-allocated once 
 *  out of scope.
 *
 *  \warning All presets presented are assumed to be valid 
 *  coordinate values.
 *
 *  \see CoordinateData
 */
class PresetWidget : public QWidget
{
   Q_OBJECT

   public:
      /** \details Default constructor.
       * 
       *  \param[in] parent The parent widget to draw onto
       */
      explicit PresetWidget(QWidget *parent = 0);
      /** \details Default destructor.
       *
       *  Handles destroying all pointers initialized by private 
       *  variables. The database connection will also be closed.
       */
      ~PresetWidget();

      /** \details Returns the SQL record at a given index.
       *
       *  Record fields of the returned object:
       *
       *       +==============+
       *       |    RECORD    |
       *       +==============+
       *       |      id      |
       *       +--------------+
       *       |  coordinate  |
       *       +--------------+
       *       | abbreviation |
       *       +--------------+
       *       | description  |
       *       +--------------+
       *
       *  \returns The QSqlRecord of preset data at index. An 
       *  empty record generally indicates an invalid index
       */
      QSqlRecord recordAt(const int index) const;
      /** \returns The total number of presets stored in the 
       *  database
       */
      int count() const;

   signals:
      /** \details This signal is emitted when a preset is chosen
       * 
       *  \param[out] preset_data A CoordinateData object of the 
       *  coordinate data values of a particular preset
       */
      void preset(CoordinateData &preset_data);
      /** \details This signal is emitted with the contents of a 
       *  preset.
       *
       *  \param[out] preset_data A QString of the coordinate 
       *  data value: coordinate + portal number, or a glyph code
       *  \param[out] preset_abbr A QString of the abbreviation 
       *  for this preset
       *  \param[out] preset_description A QString of the 
       *  description for this preset
       */
      void exportPreset(const QString &preset_data, 
                        const QString &preset_abbr, 
                        const QString &preset_description);
      /** \details This signal is emitted whenever data changes, 
       *  whether a modification, insertion, or removal.
       */
      void presetChanged();
      /** \details This signal is emitted when there is a 
       *  change to be made to the description.
       *
       *  \param[out] newText A QString value for the current preset 
       *  description text, which may be an empty string
       */
      void descriptionChange(const QString &newText);
      /** \details This signal is emitted when there is a change 
       *  to be made to the selector.
       * 
       *  Valid action values are: add, delete, change
       * 
       *  Valid values for each action:
       * 
       *       Action : Requires
       *       -----------------
       *       add : presetAbbr
       *       delete : valid index
       *       change : presetAbbr, valid index
       * 
       *  For any value not required, empty data is preferred, even 
       *  though it is ignored.
       * 
       *  \param[in] presetAbbr The preset abbreviation to change
       *  \param[in] performAction A valid action to perform with presetAbbr
       *  \param[in] index The index of presetAbbr
       */
      void selectionChange(const QString &presetAbbr, 
                           const QString &performAction, 
                           const int index);
      /** \details This signal is emitted when all values 
       *  are purged/reset within the database. Trigger for 
       *  the preset QComboBox being reset as well.
       */
      void resetSignal();
      /** \details This signal is emitted with error
       *  information, such as an invalid coordinate or 
       *  glyph code, or unable to modify/add/etc a 
       *  preset.
       *
       *  \param[out] message A QString containing 
       *  error information about the preset or action
       */
      void errorMessage(const QString &message);

   public slots:
      /** \details Adds coordinate data as a preset value.
       * 
       *  \param[in] coords A valid coordinate as a 
       *  QString in the form: "COORDINATE, portal #PORTAL NUMBER" 
       *  or "GLYPHCODE" to add to the list of presets
       *  \param[in] abbr An abbreviated/short name for the 
       *  preset, such as "GHUB" for Galactic Hub. This text 
       *  is the primary/fastest means of differentiating between 
       *  presets
       *  \param[in] description A full, more descriptive bit of text 
       *  for this preset, such as "Top of the Tree: The Galactic 
       *  Hub in Euclid" for GHUB
       *  \param[in] skipEdit Determines whether a prompt will 
       *  be shown to allow modifying preset values upon import. 
       *  By default, this value is true and does not prompt to 
       *  make changes to the preset being added. If this 
       *  value is false, then a prompt is shown to allow for
       *  setting/modifying values for the new preset.
       * 
       *  \warning There is no error-checking of the provided
       *  coordinate value.
       */
      void addPreset(const QString &coords, 
                     const QString &abbr, 
                     const QString &description,
                     const bool skipEdit = true);
      /** \details Loads preset data from an XML file.
       * 
       *  Files must have the format:
       * 
       *       <PRESETTOPLEVELBLOCK>
       *
       *          <!-- This form is essential; a single block 
       *             represents one preset -->
       *          <PRESET>
       *             <COORDINATE>0015:0034:0178:00aa, portal #8</COORDINATE>
       *             <ABBREVIATION>LV426</ABBREVIATION>
       *             <DESCRIPTION>Stay away, for your own safety.</DESCRIPTION>
       *          </PRESET>
       *
       *          <PRESET>
       *             <COORDINATE>0123456789bb</COORDINATE>
       *             <ABBREVIATION>ICRI</ABBREVIATION>
       *             <DESCRIPTION>Every time.</DESCRIPTION>
       *          </PRESET>
       *
       *       </PRESETTOPLEVELBLOCK>
       * 
       *  \param[in] file A valid filename to read preset data 
       *  from. If empty, a prompt is shown to select a file
       * 
       *  \see is_valid_coordinate, is_acceptable_glyph_code
       */
      void fromFile(const QString &file = QString());
      /** \details Removes all current preset coordinate values 
       *  from the database and the list of options.
       */
      void clear();
      /** \details Emits the signal exportPreset with the 
       *  preset data at index.
       * 
       *  If there are no presets, or the index is invalid, 
       *  then no signal is emitted.
       * 
       *  \param[in] index A valid index
       */
      void exportAt(const int index);
      /** \details Saves preset data to the provided XML file.
       * 
       *  \param[in] file A valid file to be written to. Any 
       *  existing data within said file will be overwritten
       *
       *  \see fromFile
       */
      void toFile(const QString &file = QString());
      /** \details Edits the preset with the name provided by
       *  editThisAbbr, if it exists.
       *
       *  \param[in] editThisAbbr A valid shortname for the 
       *  preset to be edited
       */
      void edit(const QString &editThisAbbr);
      /** \details Display information about the chosen preset at 
       *  the given index.
       *
       *  \param[in] index A valid index representing the preset 
       *  chosen. A number between zero and the total presets 
       *  within the database
       */
      void selectPreset(const int index);

   private:
      QSqlTableModel *presetModel;

      /** \details Initializes SQL database connection */
      void initDB();

   private slots:
      /** \details Populates the QComboBox with current preset 
       *  abbreviations.
       */
      void buildPresetList();
      /** \details Searches for the index of given preset name, 
       *  Specifically the abbreviation.
       * 
       *  \param[in] findThisPreset A valid preset abbreviation
       * 
       *  \returns The index where findThisPreset can be found, 
       *  If not found, returns the value -1
       */
      int indexOf(const QString &findThisPreset);
      /** \details Modifies the preset at index, provided 
       *  it does exist. Nothing happens if the index is 
       *  invalid.
       *
       *  \param[in] index A valid index of the preset to be 
       *  modified
       */
      void editAt(const int index);
      /** \details Verifies that the given index is a valid index
       *
       *  \returns True, if index is between 0 and total 
       *  records available
       */
      bool isValidIndex(const int index) const;
      /** \details Updates database and QComboBox information for 
       *  changes. Called anytime the presetChanged 
       *  signal is emitted.
       */
      void updatePresets();
};
//------------------------------------------------------------------
}  // end of namespace: NMS
/** @} End of doxygen group NMS */
//------------------------------------------------------------------
#endif   // __NMS_PRESET_WINDOW_H__
